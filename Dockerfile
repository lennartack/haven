# syntax=docker/dockerfile:1.9
FROM golang:1.23.0-alpine AS haven_builder
WORKDIR /go/src/haven
COPY ./haven/cli/go.mod ./haven/cli/go.sum ./
ARG TARGETOS
ARG TARGETARCH
RUN go mod download
COPY ./haven/cli .
ENV CGO_ENABLED=0
ARG VERSION=undefined
RUN go build -v \
  -ldflags "-w -s -X 'main.version=${VERSION}'" \
  -o /go/bin/haven \
  ./cmd/cli

FROM alpine:latest AS e2e_runner
RUN apk add --update curl
# Download a kubectl binary for our testing environment
RUN curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl
RUN chmod +x ./kubectl
RUN mv ./kubectl /usr/bin/kubectl
# Add our e2e tests to our testing environment
COPY ./e2e_tests.sh /usr/local/bin/
RUN chmod +x /usr/local/bin/e2e_tests.sh
# Add the haven binary built in a previous stage
COPY --link --from=haven_builder /go/bin/haven /usr/local/bin/haven
