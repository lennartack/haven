---
icon: footsteps
---

### Vertrouwd vernieuwen

Met Haven is er geen grote investering nodig om te kunnen vernieuwen. Op een laagdrempelige manier kunnen de eerste stappen worden gezet. Het is geen alles-of-niets oplossing, maar er kan stapsgewijs gewerkt worden.

Alles van Haven is open source, inclusief de hulpmiddelen en referentie implementaties. Hiermee kunnen overheden en leveranciers eenvoudig hun eigen Haven omgevingen opzetten.
