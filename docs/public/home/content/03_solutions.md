---
# If no value given, buttons won't render
button1Text: Lees meer over Haven
button1Link: /over-haven
button2Text:
button2Link:
---

## Hoe lost Haven dit op?

Haven is een generieke laag die past binnen de diversiteit van IT systemen van een gemeente. Deze laag maakt de verbinding tussen ontwikkelde applicaties en de IT infrastructuur die gemeenten gebruiken. Hiermee zijn applicaties niet meer afhankelijk van één IT infrastructuur. Applicaties kunnen overal gehost worden zonder ze aan te moeten passen aan de specifieke infrastructuur waarop ze moeten draaien.
