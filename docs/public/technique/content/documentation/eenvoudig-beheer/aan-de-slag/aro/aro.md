---
title: "Azure Red Hat OpenShift"
path: "/aan-de-slag/aro"
---

## Referentie Implementatie: Haven op Azure Red Hat OpenShift

Deze referentie implementatie verschilt van de [Red Hat OpenShift Container Platform op Azure referentie
implementatie](/techniek/aan-de-slag/openshift-op-azure) in het
feit dat onderstaande tekst een OpenShift cluster opbouwt waarop beheer zal worden
uitgevoerd door Microsoft, en de andere referentie implementatie een cluster dat door
uzelf beheerd wordt.

### Voorwaarden

Stap 1: Configureer de Azure account zoals beschreven in de [ARO
    documentatie](https://docs.microsoft.com/nl-nl/azure/openshift/tutorial-create-cluster), en zorg dat
- er geen limieten op vCPUs geconfigureerd zijn (of de limiet op minimaal 40 staat)
- er een publieke DNS zone aanwezig is in Azure om de DNS records voor het cluster in te maken

### Basis installatie

Voor de installatie van ARO volgen we de
[handleiding](https://docs.microsoft.com/nl-nl/azure/openshift/tutorial-create-cluster)
op de website van Microsoft.

Deze handleiding merkt een paar stappen aan als optioneel. Wij raden aan om deze stappen
wel te volgen. Deze stappen zijn het aanmaken van een pull secret en het aanmaken van
een DNS domein.

Door deze stappen te volgen, heeft het nieuwe OpenShift cluster direct toegang tot de
ondersteunde container images op registry.access.redhat.com en een DNS naam die
bruikbaar is voor uw applicaties.

### Naar volledige Haven compliancy
Om volledig compliant te zijn, heeft het OpenShift cluster nog een aanpassing nodig:
er moet RWX storage beschikbaar worden gemaakt.

#### RWX storage
Haven compliancy vraagt tot slot om RWX storage, dat wil zeggen: storage die door
meerdere pods tegelijk gebruikt kan worden om op te schrijven. Dit is niet op alle
platformen zonder meer beschikbaar.

Op Azure kunnen we RWX storage implementeren met de Azure File service. Hiervoor moeten
we eenmalig een aantal stappen doorlopen. Eerst maken we een storage account aan binnen
Azure:

```bash
# Vul hier de namen van een resource group, een locatie en een storage account in.
# Hoewel dit dezelfde resource group kan zijn als de resource group waarin ARO
# draait, raden we dit af: het verwijderen van het cluster met de installer verwijdert
# dan ook de storage account. Dit is niet altijd wenselijk.
export AZURE_FILES_RESOURCE_GROUP=
export AZURE_STORAGE_ACCOUNT_NAME=
export LOCATION=

az storage account create \
    --name $AZURE_STORAGE_ACCOUNT_NAME \
    --resource-group $AZURE_FILES_RESOURCE_GROUP \
    --kind StorageV2 \
    --sku Standard_LRS
```

Vervolgens wijzen we de juiste rollen rol toe aan de service principal binnen deze
resource group in Azure:
```bash
# Vul de ID in van de service principal die gebruikt is om het OpenShift cluster te
# installeren
export AZURE_SERVICE_PRINCIPAL_ID=
az role assignment create --role Contributor --assignee $AZURE_SERVICE_PRINCIPAL_ID -g $AZURE_FILES_RESOURCE_GROUP
```

Vervolgens maken we binnen OpenShift een rol aan om met Azure te kunnen communiceren:
```bash
# Vul hier het wachtwoord van de kubeadmin in, en de URL van de OpenShift API. Beide
# worden weergegeven nadat de installatie voltooid is.
export PASSWORD=$kubeadmin_pw
export APISERVER=$openshift_api_server

oc login -u kubeadmin -p $PASSWORD $APISERVER

oc create clusterrole azure-secret-reader \
    --verb=create,get \
    --resource=secrets

oc adm policy add-cluster-role-to-user azure-secret-reader system:serviceaccount:kube-system:persistent-volume-binder
```

En uiteindelijk maken we de daadwerkelijke storage class aan:
```bash
# Vul hieronder wederom de juist waarden in voor de naam van de storage account en de
# resource group.
cat << EOF >> azure-storageclass-azure-file.yaml
kind: StorageClass
apiVersion: storage.k8s.io/v1
metadata:
  name: azure-file
provisioner: kubernetes.io/azure-file
parameters:
  location: $LOCATION
  secretNamespace: kube-system
  skuName: Standard_LRS
  storageAccount: $AZURE_STORAGE_ACCOUNT_NAME
  resourceGroup: $AZURE_FILES_RESOURCE_GROUP
reclaimPolicy: Delete
volumeBindingMode: Immediate
EOF
oc create -f azure-storageclass-azure-file.yaml
```

Hier staat een [voorbeeld Ansible playbook](https://gitlab.com/commonground/haven/haven/-/tree/main/reference/openshift-on-azure)
om bovenstaande aanpassingen op een Azure account te doen. Dit playbook kan worden
uitgevoerd na de deployment van ARO zelf, en zorgt voor configuratie van de RWX
storage.

### Onderhoud

Azure Red Hat OpenShift is een managed service en native Azure service, aangeboden door
Microsoft in samenwerking met Red Hat[^1].

Doordat ARO een managed service is, hebt u zelf geen omkijken naar het life cycle
management van het OpenShift cluster, noch de onderliggende virtual machines.

Onderhoud is daarom beperkt tot de applicaties en diensten die op het OpenShift cluster
draaien.

### Vervolgstappen

Software die op OpenShift draait, wordt vaak beheerd door een operator[^2]. Vanuit de
ingebouwde OperatorHub kan additionele software worden geinstalleerd, zoals bijvoorbeeld
een Kafka cluster of serverless functionaliteit. Ook veel software van derden wordt via
de ingebouwde OperatorHub op OpenShift aangeboden, zoals geclusterde PostgreSQL
databases, API gateways, en monitoring software.

Ga voor de OperatorHub in het menu aan de linkerzijde naar Operators -> OperatorHub.

### Verder lezen

- [De volledige OpenShift documentatie](https://docs.openshift.com/container-platform/4.6/welcome/index.html)
- [OpenShift uitproberen](https://www.openshift.com/try)
- [Meer leren met hands-on labs](https://learn.openshift.com/)
- [Volledige ARO documentatie](https://docs.microsoft.com/nl-nl/azure/openshift/)

[^1]: Voor een compleet overzicht de verdeling van de verantwoordelijkheid tussen Red
  Hat, Microsoft en uzelf omtrent beheer van het cluster, zie https://docs.microsoft.com/en-us/azure/openshift/responsibility-matrix

[^2]: Zie ook: https://www.redhat.com/en/topics/containers/what-is-a-kubernetes-operator

[^3]: https://docs.microsoft.com/en-us/azure/openshift/
