---
title: "Constellation"
path: "/aan-de-slag/constellation"
---

# Constellation: Always Encrypted Kubernetes

Author: Moritz Eckert, Chief Architect, me@edgeless.systems

Constellation is a Kubernetes engine that aims to provide the best possible data security. It wraps your K8s cluster into a single *confidential context* that is shielded from the underlying cloud infrastructure. Everything inside is always encrypted, including at runtime in memory. For this, Constellation leverages confidential computing (see the [whitepaper](https://content.edgeless.systems/hubfs/Confidential%20Computing%20Whitepaper.pdf)) and more specifically Confidential VMs.

<img src="/technique/content/documentation/eenvoudig-beheer/aan-de-slag/constellation/concept.svg" alt="Concept" width="85%"/>

## Goals

From a security perspective, Constellation is designed to keep all data always encrypted and to prevent any access from the underlying (cloud) infrastructure. This includes access from datacenter employees, privileged cloud admins, and attackers coming through the infrastructure. Such attackers could be malicious co-tenants escalating their privileges or hackers who managed to compromise a cloud server.

From a DevOps perspective, Constellation is designed to work just like what you would expect from a modern Kubernetes engine.

## Use cases

Constellation provides unique security [features](https://docs.edgeless.systems/constellation/overview/confidential-kubernetes) and [benefits](https://docs.edgeless.systems/constellation/overview/security-benefits). The core use cases are:

* Increasing the overall security of your clusters
* Increasing the trustworthiness of your SaaS offerings
* Moving sensitive workloads from on-prem to the cloud
* Meeting regulatory requirements

## Next steps

You can create a Haven-compliant Constellation cluster on Azure following the [reference implementation](https://gitlab.com/commonground/haven/haven/-/tree/main/reference/constellation-on-azure).
You can learn more about the concept of Confidential Kubernetes, features, security benefits, and performance of Constellation in the [documentation](https://docs.edgeless.systems/constellation).

If you have questions, feel free to contact us: https://www.edgeless.systems/contact/.

![Constellation Shell](/technique/content/documentation/eenvoudig-beheer/aan-de-slag/constellation/shell-windowframe.svg)
