---
title: Wat is Haven
path: "/"
---

Gemeenten gebruiken applicaties voor het uitvoeren van hun taken. Die applicaties draaien op een IT infrastructuur (zoals rekenkracht, opslag en netwerkverbindingen). Iedere gemeente heeft zijn IT infrastructuur anders georganiseerd. Bij de ene gemeente draait bijvoorbeeld veel lokaal en bij de ander juist meer in de cloud. Applicaties moeten worden aangepast aan de infrastructuur waarop ze draaien. Dat maakt het voor gemeenten lastig om samen applicaties te ontwikkelen en deze snel in te zetten bij alle gemeenten.

### Haven is een standaard voor platform-onafhankelijke cloud hosting

Haven biedt daarvoor een oplossing. Concreet: Haven legt verbinding tussen ontwikkelde applicaties en iedere gemeentelijke IT infrastructuur. Met Haven kunnen gemeenten applicaties overal hosten zonder dat zij daarvoor hun IT infrastructuur hoeven aan te passen. Daardoor kunnen gemeenten applicaties samen ontwikkelen en snel inzetten bij alle gemeenten. Dit zorgt onder meer voor uniformiteit, lagere kosten en minder afhankelijkheid van leveranciers.

### Technisch

Haven schrijft een specifieke configuratie van [Kubernetes](https://www.kubernetes.io) voor die dient te worden geïmplementeerd op bestaande technische infrastructuur, bijvoorbeeld een cloud of on-premise platform.

De [voorgeschreven configuratie](/techniek/checks) zorgt ervoor dat iedere Haven omgeving functioneel gelijk is ongeacht de onderliggende technische infrastructuur. Zie het als een abstractielaag die resulteert in een gezamenlijk vertrekpunt.

Dit brengt diverse voordelen met zich mee: uniformiteit in technische infrastructuur, uitwisselbaarheid van toepassingen, leveranciersonafhankelijkheid, platformonafhankelijkheid en kostenreductie.

Implementatie van een Haven omgeving kan door gemeenten zelf worden gedaan of door een leverancier naar keuze.

## Wat is Haven niet

Haven is geen 'all inclusive' cloud oplossing, zoals bijvoorbeeld het project CBI voor ogen heeft. Haven richt zich specifiek op de hosting component, van websites en toepassingen.

Haven is ook geen security baseline. Dat kan ook niet, want security betreft de som der delen. Het is raadzaam om te kijken naar bijvoorbeeld BIO, ISO2700x en de CIS benchmarks voor het gehele platform inclusief Haven. Regelmatige penetratie testen kunnen ook een goede aanvulling zijn.

Zie voor meer informatie het werkingsgebied zoals beschreven in [de standaard](/techniek/de-standaard).

## Componenten van Haven

_De [Haven Compliancy Checker](/techniek/compliancy-checker) beschrijft en borgt de standaard door geautomatiseerd clusters te valideren. Dit is de essentie van Haven en de enige verplichting._

Ter ondersteuning zijn er verschillende [Referentie Implementaties](/techniek/aan-de-slag) beschikbaar van leveranciers en cloudproviders waarvan we hebben vastgesteld dat ze Haven Compliant zijn. Gebruik hiervan is niet verplicht.

Als extra hebben we diverse [Addons](/techniek/addons) beschikbaar gesteld om eenvoudig bijvoorbeeld NLX te installeren. Gebruik hiervan is niet verplicht.

Naast de online documentatie bevat de Haven [Gitlab repository](https://gitlab.com/commonground/haven/haven/) aanvullende technische documentatie.
