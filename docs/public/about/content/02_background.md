---
imageLink: about/content/ladder.svg
imageAlt: Illustratie van mensen die samenwerken om hogerop te komen
---

## Achtergrond

Interne teams bij VNG Realisatie liepen er tegenaan dat er technische infrastructuur ontbrak. Applicaties moesten op individuele laptops gedraaid worden. Bovenal ondervonden gemeenten beperkingen in de herbruikbaarheid van applicaties, vanwege de diversiteit in de gebruikte platformen waarop deze applicaties moesten draaien.

Vanuit de behoefte om dit beter te organiseren is Haven bedacht en direct intern bij VNG Realisatie in gebruik genomen om van te leren. Al in een vroeg stadium namen de eerste gemeenten Haven in gebruik. Haven is steeds verder uitgewerkt en inmiddels breed omarmd door gemeenten, samenwerkingsverbanden, leveranciers en intern bij VNG Realisatie.
