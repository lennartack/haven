// Copyright © VNG Realisatie 2019-2024
// Licensed under EUPL v1.2
//
import React from "react";
import { string } from "prop-types";
import { Container } from "src/components/Grid";
import HtmlContent from "src/components/HtmlContent";
import { Section, ContentCol } from "./index.styles";

const Contact = ({ content }) => (
  <Section omitArrow>
    <Container>
      <ContentCol>
        <HtmlContent content={content} />
      </ContentCol>
    </Container>
  </Section>
);

Contact.propTypes = {
  content: string,
};

export default Contact;
