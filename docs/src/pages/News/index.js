// Copyright © VNG Realisatie 2019-2024
// Licensed under EUPL v1.2
//
import React from "react";
import { object } from "prop-types";
import Head from "next/head";
import Header from "src/components/Header";
import Footer from "src/components/Footer";
import Introduction from "./Introduction";
import Community from "./Community";
import Contact from "./Contact";

const News = ({ sections }) => (
  <>
    <Head>
      <title>Haven - Nieuws</title>
      <meta name="description" content={sections.meta.description} />
    </Head>

    <Header />

    <main>
      <Introduction {...sections.introduction} />
      <Community {...sections.community} />
      <Contact {...sections.contact} />
    </main>

    <Footer />
  </>
);

News.propTypes = {
  sections: object.isRequired,
};

export default News;
