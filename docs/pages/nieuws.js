// Copyright © VNG Realisatie 2019-2024
// Licensed under EUPL v1.2
//

import { getPageSections } from "src/lib/api";
import News from "../src/pages/News";

export default function NewsPage(props) {
  return <News {...props} />;
}

export async function getStaticProps() {
  const sections = await getPageSections("news");
  return { props: { sections } };
}
