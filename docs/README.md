# Documentation

This folder contains the online documentation of Haven.


## local with development environment
Use the following commands to start the watch server:

```bash
npm install
npm run dev
```

Go to http://localhost:8000/ to view the documentation.

### Local Setup in Kubernetes

To set up a local version of the documentation pages, follow these steps:

1. Ensure `k3d` and `skaffold` are installed.
2. Verify that `checks.yaml` is in the `./docs` directory. If not, copy it manually as Skaffold can't create files on your system.

From the root directory, run:

```bash
k3d cluster create -c local/k3d-config.yaml
skaffold dev
```

Access the documentation at:

```bash
http://haven-docs.local/
```

_Note: You'll need to add an entry to `/etc/hosts` for this URL to work._

## License

Copyright © VNG Realisatie 2019-2023<br />
Licensed under EUPL v1.2
