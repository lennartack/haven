# Haven

Copyright © VNG Realisatie 2019-2023<br />
Licensed under EUPL v1.2

## Changes:

### Haven v12.1.1 - 2024-08=05

Patch changes to allow for Windows validation and addresses an issue when Pod Security Admission is enabled.

- [#811](https://gitlab.com/commonground/haven/haven/-/merge_requests/811) [Haven ] Supports Windows when validation the sha.
- [#763](https://gitlab.com/commonground/haven/haven/-/merge_requests/763) [Haven ] Fix unknowns when Pod Security Admission is enabled.
- [#779](https://gitlab.com/commonground/haven/haven/-/merge_requests/779) [RI ] Adds a reference implementation to install a Haven complaint Previder cluster.

### Haven v12.1.0 - 2024-07-22

Version v12.1.0 will be the last minor release for version 12 and only patches will be done when needed. It also means development regarding the `cli` will slow down.

- [#770](https://gitlab.com/commonground/haven/haven/-/merge_requests/770) [Haven ] Standaardregister check has been removed from the `cli` and will no longer be supported.
- [#775](https://gitlab.com/commonground/haven/haven/-/merge_requests/775) [Haven ] Previder has been added as a validated platform. Welcome Previder!

### Haven v12.0.0 - 2024-05-28

To move with the market and update Haven it has been decided to upgrade the version to `v12.0.0` which brings breaking changes to some core components.

- [#742](https://gitlab.com/commonground/haven/haven/-/merge_requests/742) [Haven] Parent issue

- [#731](https://gitlab.com/commonground/haven/haven/-/merge_requests/731) [Haven ] Add flatcar as a verified secure flavor for kubernetes:

Flatcar is a minimalistic Linux Distribution supported by large cloud vendors.

Since [flatcar](https://www.flatcar.org/) has a secure by design approach the Haven team feels it matches with the demands set for node security.

- [#737](https://gitlab.com/commonground/haven/haven/-/merge_requests/737) [Haven] Removes the LoadBalancer check

The loadbalancer check has long been a staple of the Haven compliancy check but it has been decided to remove it.
Kubernetes has grown from a platform to support most web based applications to edge, queue based nodes and many more.
Some of these do not require any ingress or loadbalancer since the cluster cannot be reached and should not be reached.
For example when using a queue from a cloud provider or on prem it is totally viable to have your Kubernetes cluster run in the background and taking things of the queue instead of being called through an api.

The other factor in this decision has been the growing number of private tunnels see for example [cloudflare private tunnel](https://developers.cloudflare.com/cloudflare-one/connections/connect-networks/private-net/).
In this way a cluster can simply be exposed by using services rather than a (cloud native) loadbalancer.

- [#743](https://gitlab.com/commonground/haven/haven/-/merge_requests/743)  Add sha checksum

It should be possible for a municipality or any user of the Haven compliancy checker to validate that the version in use has not been tempered with.
Therefor a sha checksum check has been added. Each checksum can be found along side the actual cli and can be verified by any user.

A remote sha is part of the releases that is currently checked for up to date versions of haven which will now also validate the sha.

- [#749](https://gitlab.com/commonground/haven/haven/-/merge_requests/749) [Haven ] Major version upgrades

Golang has been upgraded to version 1.22.2 which is the latest and all Kubernetes apis to version 0.30.0 which is the most recent version.


### Haven v11.1.0 - 2024-03-26
 Adds longhorn and trident as provisioners and refactors fallback method to loop over each SC
- [#731](https://gitlab.com/commonground/haven/haven/-/merge_requests/731) [Haven ] Adds Flatcar Container Linux as a valid secured nodes test since it uses Selinux.

### Haven v11.0.1 - 2024-03-12
- [#720](https://gitlab.com/commonground/haven/haven/-/merge_requests/720) [Haven ] Adds RKE as a known platform

### Haven v11.0.0 - 2023-12-20

Since this release brings a number of changes they have been divided up in a few subcategories.

We also urge all our users to check the [`README.md`](https://gitlab.com/commonground/haven/haven/-/blob/master/haven/cli/README.md?ref_type=heads) within the Haven project. It details how to use the new verified flow for anyone wanting to prove their compliant status using Standaardenregister.

#### CNCF

In this release the Haven team has changed the way we check if a cluster is CNCF compliant. 
Where we used to depend on Sonobuoy, the Haven team now depends on a maintained list of conformance certified Kubernetes flavours.
There are a few reasons that the Haven team sees for this decision.

1. Sonobuoy runs take up to 90 minutes and results are not always guaranteed
2. the CNCF keeps it's own up to date list of conformance certified Kubernetes flavours [CNCF](https://www.cncf.io/training/certification/software-conformance/)
3. Sonobuoy can be quite flaky in terms of starting and destroying instances, even when using their API

The First reason weighs quite heavy because the Haven team has always envisioned Haven as a tool that can be used frequently and easily.
Running 90 minutes test is not part of that user experience.

The Second reason is important because this opened up an alternative way for the Haven team to confirm you are running on a certified Kubernetes instance.
This does however mean that if you are running a version of Kubernetes that we currently do not support you might have a negative outcome.
We want to remedy this by making sure we test and run as often as possible on larger platforms.
We also strongly advice that if you are running on a Kubernetes flavour we currently do not support to get in touch with us.
The Haven team is more than willing to expand the list of supported platforms and write determining annotation etc for those.

The last reason has been the driving factor behind removing the CNCF checks in it's current form. Sonobuoy often fails while passing the run after.
This could be circumvented by running in the `quick` mode but this makes the check itself almost meaningless. By verifying your version and platform against a curated list the Haven team feels we struck a middle ground.
There is still a check in place and there is still the assurance that your platform adheres to CNCF conformance.

Part of this change are the following MRs:

- [#705](https://gitlab.com/commonground/haven/haven/-/merge_requests/705) [Haven ] Creates a new way to check and validate CNCF conformance as detailed above.

#### Haven

- [#685](https://gitlab.com/commonground/haven/haven/-/merge_requests/685) [Dashboard - Addons ] Dashboard and Addons have been removed from the Haven project since they no longer offer any added value.
- [#700](https://gitlab.com/commonground/haven/haven/-/merge_requests/700) [Haven ] Standaardenregister.nl has had a new release which makes it possible to create verified Haven compliant entries. These changes are reflected in the code of the `CLI`.

#### CI

- [#677](https://gitlab.com/commonground/haven/haven/-/merge_requests/677) [Haven ] Moves the pipelines away from a VM based approach to a kubernetes first approach.
- [#678](https://gitlab.com/commonground/haven/haven/-/merge_requests/678) [Haven ] E2E tests now build an environment based on AKS - GKE - EKS and runs a `haven check` on each platform before destroying it again. 
This change greatly increases our testing capabilities and quality assurance.
- [#699](https://gitlab.com/commonground/haven/haven/-/merge_requests/699) [Haven ] Documentation building and deploying has been brought into line with other Haven projects.

#### Internal

- [#691](https://gitlab.com/commonground/haven/haven/-/merge_requests/691) [Haven ] The E2E flag is no longer supported.
- [#694](https://gitlab.com/commonground/haven/haven/-/merge_requests/694) [Docs ] A new section called News has been added to stay up to date with the Haven community!
- [#702](https://gitlab.com/commonground/haven/haven/-/merge_requests/702) [Haven ] Since this release has brought along quite some changes a lot of files and packages have been removed that were no longer needed.

### Haven v10.6.1 - 2023-10-25
- [#674](https://gitlab.com/commonground/haven/haven/-/merge_requests/674) [Haven ] Optimizes the way Haven shows help for config commands

### Haven v10.6.0 - 2023-10-19
- #639 [Haven ] Adds warning on deprecation of dashboard and addons
- #633 [Haven ] Implements config command so people can identify their telemetry calls

### Haven v10.5.1 - 2023-10-04
- #666 [Haven ] Replaces jawher/mow.cli with cobra
- #667 [Haven ] Fixes output muddling by third part tools during progress bar progress

### Haven v10.5.0 - 2023-09-06

- #543 [RI    ] Upgrades OpenStack reference implementation Kops from 1.26.x to 1.27.x
- #617 [RI    ] Fixes OpenStack reference implementation docker tag
- #614 [Haven ] Adds basic telemetry which enables basic Haven standard adoption insights

### Haven v10.4.0 - 2023-06-07

- #590 [Haven ] Bugfixes Sonobuoy detection on Windows
- #586 [Haven ] Rebuilds Haven CRD
- #584 [Haven ] Improves CLI context reliability and speeds up checks in parallel
- #543 [RI    ] Upgrades OpenStack reference implementation Kops from 1.25.x to 1.26.x
- #500 [Addons] Adds platform details tile to Haven Dashboard
- #438 [Haven ] Adds Kubescape as a suggested check

### Haven v10.3.0 - 2023-01-17

- #575 [Haven ] Cleans up CIS benchmark notification about child pods
- #560 [Haven ] CLI binary size reduced by removing more symbols and refactoring the built-in Kubernetes client
- #568 [RI    ] OpenStack Reference Implementation now has it's own version separately from the main Haven version
- #567 [Haven ] CLI development builds now have improved semantic versioning compatibility

### Haven v10.2.0 - 2022-12-27

- #562 [Docs  ] Adds separate operator readme
- #565 [Haven ] Adds "haven version" subcommand
- #558 [Docs  ] Adds list of publications to the "Over Haven" section in the docs
- #487 [Haven ] Removes Flux from Haven Dashboard in favor of using Helm directly
- #548 [Haven ] Fixes broken Haven add-ons
- #549 [Haven ] Removes deprecated Haven dashboard suggested check
- #538 [Haven ] Adds Haven Compliancy Checker version self-check
- #514 [Docs  ] Adds "Handreiking programma van eisen Haven" to docs
- #522 [Haven ] Ensure RHEL8 selinux compatibility

### Haven v10.1.0 - 2022-07-26

- #333 [RI    ] Adds new Amazon AWS Haven Reference Implementation
- #501 [RI    ] Updates Haven on OpenStack RI from Kops 1.22.x to 1.24.x
- #533 [Haven ] Updates LKRG detection
- #524 [Haven ] Adds Talos Linux compatibility to Haven Compliancy Checker removing privileged pod usage
- #526 [Haven ] Adds more unit tests to Haven Compliancy Checker code
- #512 [Haven ] Fixes Haven Compliancy Checker SELinux false positive on Bottlerocket AWS AMI's
- #508 [Docs  ] Improved docs with preperation steps
- #494 [Addons] Adds Haven Compliancy status to the Haven Dashboard addon
- #451 [RI    ] Updates Haven on OpenStack RI from Kops 1.20.x to 1.22.x
- #484 [Haven ] Fixes Haven Compliancy Checker to run on Rancher clusters
- #473 [Haven ] Moves whitelisted checks from code to yaml

### Haven v10.0.0 - 2022-02-07 - Haven has been designated as a standard for platform independent cloud hosting at Dutch municipalities

- #405 [Haven ] Improves application of Sonobuoy external CNCF checks including progress bar
- #468 [Haven ] Fixes CIS benchmarks running of kube-bench on GKE
- #491 [Haven ] Adds whitelist to yaml vs hard coding it in the Haven Compliancy Checker
- #462 [Haven ] Adds argument to specify namespace used by Haven Compliancy Checker
- #477 [Haven ] Fixes Haven Compliancy Checker runs on known platforms behind a proxy
- #429 [Haven ] CRD improvements: contract based, structured fields instead of JSON blob + generate structs #489
- #329 [Addons] Adds NLX addon for easy demo installations
- #475 [Haven ] Fixes flaky privileged pod check
- #456 [RI    ] Fixes AKS CNCF validation issue
- #459 [Docs  ] Updated website content
- #400 [Docs  ] New website structure with improved design and Compliancy Overview
- #358 [Docs  ] Adds VMware Reference Implementation
- #294 [Addons] Cert-manager addon provisions Lets Encrypt ClusterIssuer, monitoring addon configures Grafana OIDC
- #409 [Haven ] Adds detected kubernetes version to logging output in Haven Compliancy Checker
- #335 [Docs  ] Document standard change
- #401 [Docs  ] Document release cycle

### Haven v9.0.0 - 2021-08-03

- #428 [RI    ] Upgrades Haven OpenStack Reference Implementation kops version from 1.19 to 1.20.
- #376 [Haven ] Adds privileged pod flow to Haven Compliancy Checker replacing SSH requirement
- #271 [RI    ] Phases out KubeDB
- #408 [Haven ] Adds Haven CRD allowing Haven Compliancy Checker to persist results in the cluster
- #423 [Docs  ] Fixes font 404's in docs page

### Haven v8.0.0 - 2021-05-12

- #350 [RI    ] Upgrades Haven OpenStack Reference Implementation kops version from 1.18 to 1.19.
- #393 [Haven ] Configures Sonobuoy pre-flight checks with platform specifics
- #396 [Haven ] Adds ARO logging detection
- #406 [Docs  ] Fixes layout of code blocks in docs
- #407 [Addons] Removes Postgres Operator from legacy addons
- #397 [Addons] Adds Postgres Operator to addons with Haven CLI
- #394 [Haven ] Adds OpenShift platform detection to Haven Compliancy Checker
- #374 [RI    ] Fixes protokube multicluster in single project bug
- #389 [Haven ] Updates check wit new master labels
- #384 [Docs  ] Fixes layout of Checks
- #369 [Haven ] Show Checks rationale on the website using single source of truth
- #371 [Docs  ] Updates docs / README's with latest insights
- #373 [Docs  ] Fixes docs images/code blocks on the website

### Haven v7.0.0 - 2021-03-17

- #351 [Haven ] Display error when not able to parse Kubernetes configuration
- #352 [Haven ] Adds JSON output to the CLI and moves logging to top level
- #287 [Addons] Improve Haven dashboard UX
- #357 [Haven ] Adds rationale output for Compliancy Checks
- #353 [Docs  ] Adds new Haven video to online docs
- #371 [Docs  ] Update docs/READMEs with latest insights
- #369 [Docs  ] Add rationale of checks to documentation page

### Haven v6.0.0 - 2021-01-25

- #304 [Chore ] Shuffle: re-aligns the repository layout and documentation with the state of Haven
- #309 [Haven ] Adds CIS as an opt-in external suggested check

### Haven v5.1.0 - 2021-01-12

- #332 [Haven ] Updates compliancy checks like whitelisting GKE SSL automation
- #318 [Haven ] Fixes the external ip check on GKE (response empty vs none)
- #283 [Addons] Initial work on OPA gatekeeper
- #300 [Haven ] Updates compliancy checks like whitelisting OpenShift SSL automation
- #308 [Haven ] Adds separate suggestions table to Haven Compliancy Checker output
- #280 [Docs  ] Improved Azure documentation including Terraform example (Azure AD, Azure CNI, Terraform)
- #234 [Docs  ] Add Amazon EKS documentation
- #302 [Haven ] Fixed naming of 'addons' (used in multiple contexts), suggest instead of require Haven Dashboard
- #270 [Addons] Helm chart for NetworkPolicy, LimitRange and ResourceQuota in namespace
- #288 [Addons] Add cert-manager, ingress-nginx and monitoring to Haven CLI addons

### Haven v5.0.1 - 2020-11-16
- #293 Fix link to oauth2-proxy chart as it is deprecated in the stable repository
- #291 Fix link to Dex Helm chart as it is deprecated in the stable repository

### Haven v5.0.0 - 2020-11-12
- #277 Improved AKS Compliancy checks
- #161 Addons installable from the Haven CLI
- #273 Automated build and deployment of new releases

### Haven v4.4 - 2020-11-04
- #249  Added a check to Haven Compliancy Checker confirming Kubernetes version is up to date
- #267  Removed the Haven Operator in line with documentation using Flux
- #263  Added a check to Haven Compliancy Checker confirming LoadBalancer service type
- #254  Added AppArmor as an alternative to the SELinux/Grsecurity/LKRG check
- #248  Added documentation about deploying a Web Application Firewall (WAF) in your Haven cluster
- #255  Restructured Haven Compliancy Checker categories e.g. clustered addons into single category
- #262  Clarified documentation regarding e.g. Haven Compliancy start vs finish and database usage

### Haven v4.3 - 2020-10-27
- #123  Upgrade Haven with Kops 1.18.2, fixing kops-controller reauth + Octavia cidr whitelisting and replacing our /v2.0/v2.0 octavia patches
- #252  Fixed incorrect number of master nodes HCC check in certain cases
- #247  Fixed Prometheus volume cleanup retention
- #232  Refactored some Haven Compliancy Checkes through user feedback
- #230  Removes sniStrict settings because it does not work with default certificate
- #224  Automatically enforces HSTS on all ingress resources
- #141  Tunes SSL settings of Traefik addon

### Haven v4.2 - 2020-09-25
- #202  Adds Monitoring addon replacing logging + metrics + netchecker addons, switched from ElasticSearch to Loki
- #160  Adds proper cleanup when HCC is interrupted during CNCF checks
- #216  Adds LKRG next to SELinux and Grsec HCC check
- #209  Fixes etcd-manager backup failures upstream (gophercloud reauth) preparing for Kops 1.18 upgrade to solve this
- #188  Improves OpenID token refresh workflow

### Haven v4.1 - 2020-08-25
- #213  Make Haven CLI more compatible
- #219  Upgrade cert-manager to 0.16.1
- #215  Compliancy checker: change AppStore check to Haven Dashboard
- #190  Compliancy checker: Fix correct master and worker node detection on AWS EKS, Azure AKS and Google GKE
- #189  Make it easy to run Haven Compliancy Checker outside Haven Docker image / Add documentation page
- #206  Document setting up Azure AKS
- #173  Upgrade Haven with Kops 1.17.1

### Haven v4.0 - 2020-07-29
- #183  Fixes CSI to run on master nodes
- #163  Adds resource limits to all addons
- #178  Adds Haven Dashboard
- #168  Adds High Availability to Traefik
- #35   Adds support for multiple node pools
- #143  Fixes iptables legacy mode host OS compatibility
- #147  Adds Flux operator (includes #152, #153 and #174)
- #131  Adds Netchecker addon
- #70   Adds Kubernetes Dashboard addon
- #130  Adds ElasticSearch cleanup job using Curator
- #72   Adds online Haven Documentation pages at https://haven.commonground.nl/ (includes #154, #156 and #198)
- #128  Adds Postgres Operator addon

### Haven v3.3 - 2020-04-15
- #127  Adds more HCC checks (mostly HA related).

### Haven v3.2 - 2020-04-08
- #79   Adds more HCC checks including CNCF (integrating sonobuoy).
- #110  Upgrades Devstack leveraging Virtualbox VT-x.

### Haven v3.1 - 2020-03-11
- #106  Upgrades kops version from 1.17a4 to 1.17b1.
- #109  Fixes traefik loadbalancer name uniqueness with multiple clusters in a single project.

### Haven v3.0 - 2020-02-26
- #77   [Major change] Add 'HCC' (Haven Compliancy Checker) checks: part 1.
- #82   [Major and breaking change] Replaces Kubespray with Kops as "Referentie Implementatie" to deploy and maintain clusters.
- #97   Adds usage of external Cloud Controller Manager and Cinder provisioner.
- #96   Adds Customizable master and node volume sizes.
- #95   Adds in-cluster LB support for Octavia CIDR and fixes gophercloud.
- #94   Adds API LB support for Octavia CIDR and fixes gophercloud.
- #91   Upgrades Helm from 2.x to 3.x.
- #84   Bash completion.
- #85   Refactors secrets management to not use a single binary blob anymore.

### Haven v2.3 - 2020-01-15
- #45   Kops PoC succesful.

### Haven v2.2 - 2019-11-26
- #58   Adds OpenID Connect based user management.

### Haven v2.1 - 2019-09-19
- #67   Adds user management to the ElasticSearch stack.
- #65   Adds metrics via Prometheus and Grafana.

### Haven v2.0 - 2019-09-05
- #17   Adds monitoring through Logging (ElasticSearch, Kibana, Fluentd) and Alerting (ElastAlert).

### Haven v1.5 - 2019-08-28
- #63   Fixes Weave CNI on CoreOS networking bug.

### Haven v1.4 - 2019-07-31
- #61   Adds improved kube-controller-manager patch.

### Haven v1.3 - 2019-07-30
- #31   Adds horizontal pod autoscaling.
- #27   Adds rolling cluster upgrades.

### Haven v1.2 - 2019-07-22
- #46   Fixes first users issues (envs location, haven.sh location, docs, env handover, layout).
- #48   Adds automatic publication of Haven docker images.
- #51   Fixes $HOME to equal WORKDIR in Haven container.

### Haven v1.1 - 2019-07-15
- #43   Fixes 'clear security group rules' issue of kube-controller-manager.
- #50   Fixes terraform 0.12.x kubespray incompatibility.
- #12   Adds initial gitlab runners setup.

### Haven v1.0 - 2019-06-27
- #23   Adds public dashboard.
- #42   Adds infra update script.
- #40   Adds octavia lbaas support.
- #38   Adds improved deployments (R2 cinder, kubedb, fixes).
- #36   Adds environment gpg encryption at rest.
- #32   Adds persistent volume support (RWO).
- #29   Adds kubernetes user management.
- #34   Adds metrics-server.
- #26   Adds traefik automated SSL deployments.
- #25   Adds helm deployments.
- #7    Proof of Concept Haven: Kubernetes cluster on OpenStack, private topology with bastion/lbaas, RBAC enabled.
