import { Aws, StackProps } from "aws-cdk-lib";
import { ICluster } from "aws-cdk-lib/aws-eks";
import { Policy, PolicyStatement } from "aws-cdk-lib/aws-iam";
import { NagSuppressions } from "cdk-nag";
import { Construct } from "constructs";

export interface ClusterAutoscalerProps extends StackProps {
  cluster: ICluster;
}

/**
 * Install cluster autoscaler
 *
 * @see https://aws.github.io/aws-eks-best-practices/cluster-autoscaling/
 */
export class ClusterAutoscaler extends Construct {
  readonly namespace = "cluster-autoscaler";

  constructor(scope: Construct, id: string, props: ClusterAutoscalerProps) {
    super(scope, id);

    // Create namespace for CA
    const ns = props.cluster.addManifest("CANamespace", {
      apiVersion: "v1",
      kind: "Namespace",
      metadata: {
        name: this.namespace,
      },
    });

    // IRSA setup
    const sa = props.cluster.addServiceAccount("cluster-autoscaler", {
      namespace: this.namespace,
    });

    sa.node.addDependency(ns);

    const caPolicy = new Policy(this, "CAPolicy", {
      roles: [sa.role],
      statements: [
        new PolicyStatement({
          actions: [
            "autoscaling:DescribeAutoScalingGroups",
            "autoscaling:DescribeAutoScalingInstances",
            "autoscaling:DescribeLaunchConfigurations",
            "autoscaling:DescribeTags",
            "ec2:DescribeLaunchTemplateVersions",
          ],
          resources: ["*"],
        }),
        new PolicyStatement({
          actions: [
            "autoscaling:SetDesiredCapacity",
            "autoscaling:TerminateInstanceInAutoScalingGroup",
            "autoscaling:UpdateAutoScalingGroup",
          ],
          resources: ["*"],
          conditions: {
            StringEquals: {
              "autoscaling:ResourceTag/k8s.io/cluster-autoscaler/enabled":
                "true",
            },
          },
        }),
      ],
    });

    const chart = props.cluster.addHelmChart("CAHelm", {
      chart: "cluster-autoscaler-chart",
      release: "ca",
      repository: "https://kubernetes.github.io/autoscaler",
      namespace: this.namespace,
      createNamespace: false,
      values: {
        autoDiscovery: {
          clusterName: props.cluster.clusterName,
        },
        awsRegion: Aws.REGION,
        rbac: {
          serviceAccount: {
            create: false,
            name: sa.serviceAccountName,
            annotations: {
              "eks.amazonaws.com/role-arn": sa.role.roleArn,
            },
          },
        },
      },
    });
    chart.node.addDependency(ns);

    NagSuppressions.addResourceSuppressions(caPolicy, [
      {
        id: "AwsSolutions-IAM5",
        reason:
          "The autoscaling policy broadly defines access to all autoscalers. Refine as needed.",
      },
    ]);
  }
}
