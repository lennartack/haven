import { Aws, Stack } from "aws-cdk-lib";
import {
  InstanceClass,
  InstanceSize,
  InstanceType,
  LaunchTemplate,
  MachineImage,
  UserData,
} from "aws-cdk-lib/aws-ec2";
import { Cluster, Nodegroup } from "aws-cdk-lib/aws-eks";
import { NagSuppressions } from "cdk-nag";
import { Construct, IConstruct } from "constructs";

export interface UbuntuEksNodeGroupProps {
  cluster: Cluster;
  image: string;
  desiredSize: number;
}

export class UbuntuEksNodeGroup extends Construct {
  readonly launchTemplate: LaunchTemplate;
  readonly nodegroup: Nodegroup;
  readonly nodegroupInternalName: string;

  constructor(scope: IConstruct, id: string, props: UbuntuEksNodeGroupProps) {
    super(scope, id);

    this.nodegroupInternalName = "ubuntu";

    const userData = UserData.forLinux();
    userData.addCommands(
      "set -o xtrace",
      `/etc/eks/bootstrap.sh ${props.cluster.clusterName} --kubelet-extra-args '--node-labels=eks.amazonaws.com/nodegroup=${this.nodegroupInternalName},eks.amazonaws.com/nodegroup-image=${props.image}'`,
      `/opt/aws/bin/cfn-signal --exit-code $? --stack  ${Aws.STACK_NAME} --resource NodeGroup --region ${Aws.REGION}`
    );

    this.launchTemplate = new LaunchTemplate(this, "LaunchTemplate", {
      machineImage: MachineImage.genericLinux({
        [Stack.of(this).region]: props.image,
      }),
      instanceType: InstanceType.of(InstanceClass.T3, InstanceSize.MEDIUM),
      requireImdsv2: true, // @see https://aws.github.io/aws-eks-best-practices/security/docs/iam/#restrict-access-to-the-instance-profile-assigned-to-the-worker-node
      userData,
    });

    this.nodegroup = props.cluster.addNodegroupCapacity(
      this.nodegroupInternalName,
      {
        nodegroupName: this.nodegroupInternalName,
        desiredSize: props.desiredSize,
        launchTemplateSpec: {
          id: this.launchTemplate.launchTemplateId!,
          version: this.launchTemplate.latestVersionNumber,
        },
      }
    );

    // Infra checker suppressions
    NagSuppressions.addResourceSuppressionsByPath(
      Stack.of(this),
      `${this.nodegroup.node.path}/NodeGroupRole`,
      [
        {
          id: "AwsSolutions-IAM4",
          reason: "Standard managed policies are used for EKS worker nodes.",
          appliesTo: [
            "Policy::arn:<AWS::Partition>:iam::aws:policy/AmazonEKSWorkerNodePolicy",
            "Policy::arn:<AWS::Partition>:iam::aws:policy/AmazonEKS_CNI_Policy",
            "Policy::arn:<AWS::Partition>:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly",
          ],
        },
      ],
      true
    );
  }
}
