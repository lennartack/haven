import { CfnJson } from "aws-cdk-lib";
import { ICluster, KubernetesPatch } from "aws-cdk-lib/aws-eks";
import { Role, FederatedPrincipal, ManagedPolicy } from "aws-cdk-lib/aws-iam";
import { NagSuppressions } from "cdk-nag";
import { Construct, IConstruct } from "constructs";

export interface EnableIrsaProps {
  cluster: ICluster;
}

/**
 * @see https://github.dev/aws/aws-eks-best-practices/tree/master/projects/enable-irsa/src
 */
export class EnableIrsa extends Construct {
  readonly role: Role;

  constructor(scope: IConstruct, id: string, props: EnableIrsaProps) {
    super(scope, id);

    const oidcIssuer =
      props.cluster.openIdConnectProvider.openIdConnectProviderIssuer;

    const stringEquals = new CfnJson(this, "ConditionJson", {
      value: {
        [oidcIssuer + ":aud"]: "sts.amazonaws.com",
        [oidcIssuer + ":sub"]: "system:serviceaccount:kube-system:aws-node",
      },
    });

    this.role = new Role(this, "VpcCniRole", {
      roleName: "haven-eks-vpc-cni",
      assumedBy: new FederatedPrincipal(
        props.cluster.openIdConnectProvider.openIdConnectProviderArn,
        {
          StringEquals: stringEquals,
        },
        "sts:AssumeRoleWithWebIdentity"
      ),
    });

    this.role.addManagedPolicy(
      ManagedPolicy.fromAwsManagedPolicyName("AmazonEKS_CNI_Policy")
    );

    NagSuppressions.addResourceSuppressions(this.role, [
      { id: "AwsSolutions-IAM4", reason: "Using managed CNI role" },
    ]);

    new KubernetesPatch(this, "UpdateAwsNodeServiceAccount", {
      cluster: props.cluster,
      resourceNamespace: "kube-system",
      resourceName: "serviceaccount/aws-node",
      applyPatch: {
        metadata: {
          annotations: {
            "eks.amazonaws.com/role-arn": this.role.roleArn,
          },
        },
      },
      restorePatch: {},
    });

    new KubernetesPatch(this, "UpdateAwsNodeDaemonSet", {
      cluster: props.cluster,
      resourceNamespace: "kube-system",
      resourceName: "daemonset/aws-node",
      applyPatch: {
        spec: {
          template: {
            metadata: {
              annotations: {
                irsa: "enabled",
              },
            },
          },
        },
      },
      restorePatch: {},
    });
  }
}
