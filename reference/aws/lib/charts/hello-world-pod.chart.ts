import { Chart } from "cdk8s";
import { Pod, ServiceAccount } from "cdk8s-plus-21";
import { IConstruct } from "constructs";

export interface HelloWorldPodChartProps {
  serviceAccountName: string;
}

export class HelloWorldPodChart extends Chart {
  constructor(scope: IConstruct, id: string, props: HelloWorldPodChartProps) {
    super(scope, id);

    const serviceAccount = ServiceAccount.fromServiceAccountName(
      props.serviceAccountName
    );

    new Pod(this, "HelloWorld", {
      metadata: { name: "hello-world" },
      serviceAccount,
      containers: [
        {
          name: "hello",
          image: "paulbouwer/hello-kubernetes:1.5",
          port: 8080,
        },
      ],
    });
  }
}
