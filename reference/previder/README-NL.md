---
title: "Previder Kubernetes Engine"
path: "/aan-de-slag/previder"
---

## Referentie Implementatie: Haven op Previder Kubernetes Engine

Je kan eenvoudig Kubernetes uitrollen op Previder IaaS door gebruik te maken van [PKE](https://previder.nl/diensten/kubernetes-as-a-service).

**Vertrouwd en lokaal**

Je kunt jouw applicaties snel en eenvoudig laten landen op ons stabiele cloudplatform, vanuit onze eigen [high-end datacenters in Nederland](https://previder.nl/diensten/datacenter). Daarnaast leveren onze specialisten hierop zelf de support.

**Redundantie**

Met Kubernetes as a Service is het nu mogelijk clusters tegelijk uit te rollen over de datacenterlocaties van Previder.

### Voorwaarden

* Een geverifieerd account op https://portal.previder.nl
* De volgende software moet beschikbaar zijn:
  * kubectl
  * opentofu / terraform
* Er moet NFS opslag beschikbaar zijn binnen het netwerk. Bij voorkeur Previder STaaS (https://portal.previder.nl/#/storage/staas)

### Installatie

#### From scratch
1. Volg de stap-voor-stap [handleiding](https://github.com/previder/kubernetes-examples/blob/f92ca670522be04083e0a53180554c8cb344ad83/docs/installatiehandleiding-kubernetes-cluster.pdf) voor de installatie van een cluster.

#### Binnen een bestaande omgeving

1. Login op https://portal.previder.nl
2. Navigeer in het menu naar Kubernetes en kies voor Cluster
3. Druk op de knop "Nieuw cluster"
  * Stap 1: Kies een cluster type (bijv. Express)
  * Stap 2: Schakel "Hoge beschikbaarheid" in en zorg ervoor dat "Geavanceerde instellingen" is uitgeschakeld.
  * Stap 3: Kies voor de worker sizing Medium
  * Stap 4: 
    * Voer een naam voor het cluster in
    * Schakel Automatische updates uit, en selecteer versie 1.30.x. (of een andere [Gecertificeerde Kubernetes](https://github.com/cncf/k8s-conformance) versie)
  * Stap 5: 
    * Bij "Installeer een CNI in het cluster" selecteer je "Calico"
    * Vink de optie "Ik heb een netwerk aangemaakt ..." aan, en druk op knop "Selecteer netwerk".
    * Selecteer het netwerk waarin het cluster uitgerold moet worden
    * Voer een VIP IPv4 adres in waarop het cluster binnen dit netwerk bereikbaar moet worden.
  * Druk op de knop "Aanmaken"
  * Navigeer in de Previder Portal naar het cluster, en download de kubeconfig vanuit het tabblad Endpoints.

### Configuratie
1. Gebruik OpenTofu om de vereiste software te installeren in je cluster. Om dit te doen gebruik je de kubeconfig die je eerder hebt gedownload.
   * Navigeer naar de [terraform/configuration](./terraform/configuration) folder.
   * Pas het bestand variables.tf aan
     * Specificeer de locatie van de kubeconfig (standaard: "~/.kube/config") in het `kube_config` variabelen blok
     * Specificeer de NFS server en het NFS pad in het `storage` variabelen blok
   * Voer de volgende commando's uit
```shell
tofu init
tofu plan
```
Controleer dat er geen fouten voorkomen. Voer vervolgens het volgende commando uit om de software te installeren in je cluster.
```shell
tofu apply
```
2. Voer de Haven Compliancy checker uit. Installatieinstructies kun je [here](https://haven.commonground.nl/techniek/compliancy-checker) vinden.

### Result
```shell
[I] Results: 16 out of 16 checks passed, 0 checks skipped, 0 checks unknown. This is a Haven Compliant cluster.

[I] Compliancy checks results:

+----------------+--------------------------------------------------------------------------+--------+
|    CATEGORY    |                                   NAME                                   | PASSED |
+----------------+--------------------------------------------------------------------------+--------+
| Fundamental    | Self test: HCC version is latest major or within 3 months upgrade window | YES    |
| Fundamental    | Self test: does HCC have cluster-admin                                   | YES    |
| Infrastructure | Multiple availability zones in use                                       | YES    |
| Infrastructure | Running at least 3 master nodes                                          | YES    |
| Infrastructure | Running at least 3 worker nodes                                          | YES    |
| Infrastructure | Nodes have SELinux, Grsecurity, AppArmor, LKRG, Talos or Flatcar enabled | YES    |
| Infrastructure | Private networking topology                                              | YES    |
| Cluster        | Kubernetes version is latest stable or max 3 minor versions behind       | YES    |
| Cluster        | Role Based Access Control is enabled                                     | YES    |
| Cluster        | Basic auth is disabled                                                   | YES    |
| Cluster        | ReadWriteMany persistent volumes support                                 | YES    |
| External       | CNCF Kubernetes Conformance                                              | YES    |
| Deployment     | Automated HTTPS certificate provisioning                                 | YES    |
| Deployment     | Log aggregation is running                                               | YES    |
| Deployment     | Metrics-server is running                                                | YES    |
| Validation     | SHA has been validated                                                   | YES    |
+----------------+--------------------------------------------------------------------------+--------+

[I] Suggested checks results:

+----------+-----------------------------------+---------+
| CATEGORY |               NAME                | PASSED  |
+----------+-----------------------------------+---------+
| External | CIS Kubernetes Security Benchmark | SKIPPED |
| External | Kubescape                         | SKIPPED |
+----------+-----------------------------------+---------+

```

#### Infrastructuur als code

Het is mogelijk om middels OpenTofu een cluster aan te maken. Meer informatie is [hier](./terraform/infrastructure) te vinden.
