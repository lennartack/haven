provider "helm" {
  kubernetes {
    config_path    = var.kube_config.path
    config_context = var.kube_config.context
  }
}