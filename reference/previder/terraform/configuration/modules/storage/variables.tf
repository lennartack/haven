variable "storage_enabled" {
  description = "Enable Previder STaaS"
  type        = bool
}

variable "storage_name" {
  description = "Name of the instance"
  type        = string
}

variable "storage_repository" {
  description = "Repository URL"
  type        = string
}

variable "storage_chart" {
  description = "Helm Chart"
  type        = string
}

variable "storage_chart_version" {
  description = "Helm Chart version"
  type        = string
}

variable "storage_create_namespace" {
  description = "Create namespace"
  type        = bool
}

variable "storage_namespace" {
  description = "Namespace name"
  type        = string
}

variable "storage_nfs_server" {
  description = "IP address of the NFS server"
  type        = string
}

variable "storage_nfs_path" {
  default = "Path on the NFS server"
  type    = string
}

variable "storage_nfs_storage_class" {
  description = "New name for the storage class"
  type        = string
}

