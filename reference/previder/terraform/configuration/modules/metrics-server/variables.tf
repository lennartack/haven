variable "metrics_server_enabled" {
  description = "Enable Metrics server"
  type        = bool
}

variable "metrics_server_name" {
  description = "Name of the instance"
  type        = string
}

variable "metrics_server_repository" {
  description = "Repository URL"
  type        = string
}

variable "metrics_server_chart" {
  description = "Helm Chart"
  type        = string
}

variable "metrics_server_chart_version" {
  description = "helm Chart version"
  type        = string
}

variable "metrics_server_create_namespace" {
  description = "Create namespace"
  type        = bool
}

variable "metrics_server_namespace" {
  description = "Namespace name"
  type        = string
}

variable "metrics_server_deployment_replicas" {
  description = "Number of replicas in the deployment"
  type        = number
}

variable "metrics_server_extra_args" {
  description = "Extra command arguments"
  type = list(string)
}