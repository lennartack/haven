resource "helm_release" "metrics-server" {
  count            = var.metrics_server_enabled ? 1 : 0
  name             = var.metrics_server_name
  repository       = var.metrics_server_repository
  chart            = var.metrics_server_chart
  version          = var.metrics_server_chart_version
  namespace        = var.metrics_server_namespace
  create_namespace = var.metrics_server_create_namespace

  dynamic set {
    for_each = var.metrics_server_extra_args
    content {
      name = join(".", ["args", set.key])
      value = set.value
    }
  }

  set {
    name  = "replicas"
    value = var.metrics_server_deployment_replicas
  }
}