variable "logging_enabled" {
  description = "Enable FluentD"
  type        = bool
}

variable "logging_name" {
  description = "Name of the instance"
  type        = string
}

variable "logging_repository" {
  description = "Repository URL"
  type        = string
}

variable "logging_chart" {
  description = "Helm Chart"
  type        = string
}

variable "logging_chart_version" {
  description = "Helm Chart version"
  type        = string
}

variable "logging_create_namespace" {
  description = "Create namespace"
  type        = bool
}

variable "logging_namespace" {
  description = "Namespace name"
  type        = string
}

