module "kubernetes_cluster" {
  source                                          = "./modules/kubernetes-cluster"
  kubernetes_cluster_enabled                      = var.kubernetes_cluster.enabled
  kubernetes_cluster_name                         = var.kubernetes_cluster.name
  kubernetes_cluster_cni                          = var.kubernetes_cluster.cni
  kubernetes_cluster_network                      = var.kubernetes_cluster.network
  kubernetes_cluster_vips                         = var.kubernetes_cluster.vips
  kubernetes_cluster_endpoints                    = var.kubernetes_cluster.endpoints
  kubernetes_cluster_version                      = var.kubernetes_cluster.version
  kubernetes_cluster_compute_cluster              = var.kubernetes_cluster.compute_cluster
  kubernetes_cluster_high_available_control_plane = var.kubernetes_cluster.high_available_control_plane

  kubernetes_cluster_auto_update        = var.kubernetes_cluster.auto_update
  kubernetes_cluster_auto_scale_enabled = var.kubernetes_cluster.auto_scale_enabled
  kubernetes_cluster_minimal_nodes      = var.kubernetes_cluster.minimal_nodes
  kubernetes_cluster_maximal_nodes      = var.kubernetes_cluster.maximal_nodes

  kubernetes_cluster_control_plane_cpu_cores  = var.kubernetes_cluster.control_plane_cpu_cores
  kubernetes_cluster_control_plane_memory_gb  = var.kubernetes_cluster.control_plane_memory_gb
  kubernetes_cluster_control_plane_storage_gb = var.kubernetes_cluster.control_plane_storage_gb

  kubernetes_cluster_node_cpu_cores  = var.kubernetes_cluster.node_cpu_cores
  kubernetes_cluster_node_memory_gb  = var.kubernetes_cluster.node_memory_gb
  kubernetes_cluster_node_storage_gb = var.kubernetes_cluster.node_storage_gb

  previder_config_url      = var.previder_config.url
  previder_config_customer = var.previder_config.customer
  previder_config_token    = var.previder_config.token
}

# Virtual network example
module "virtual_network" {
  source                  = "./modules/virtual-network"
  virtual_network_enabled = var.virtual_network.enabled
  virtual_network_name    = var.virtual_network.name
  virtual_network_type    = var.virtual_network.type
  virtual_network_group   = var.virtual_network.group

  previder_config_url      = var.previder_config.url
  previder_config_customer = var.previder_config.customer
  previder_config_token    = var.previder_config.token
}
#
# STaaS environment exampple
# !!! NOT IMPLEMENTED YET !!!
# module "staas_environment" {
#   source                   = "./modules/staas-environment"
#   previder_config_url      = var.previder_config.url
#   previder_config_customer = var.previder_config.customer
#   previder_config_token    = var.previder_config.token
# }