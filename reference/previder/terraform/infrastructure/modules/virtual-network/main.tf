resource "previder_virtual_network" "previder" {
  count = var.virtual_network_enabled ? 1 : 0
  name  = var.virtual_network_name
  type  = var.virtual_network_type
  group = var.virtual_network_group
}