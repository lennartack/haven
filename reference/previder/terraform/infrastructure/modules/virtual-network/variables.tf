variable "virtual_network_enabled" {
  description = "Enable Virtual network"
  type        = bool
}

variable "virtual_network_name" {
  description = "Name of the network"
  type        = string
}

variable "virtual_network_type" {
  description = "Type (IAN, VLAN, SEGMENT, VLAN_TRUNK)"
  type        = string
}

variable "virtual_network_group" {
  description = "ID or name of the group this network is part of"
  type        = string
}

variable "previder_config_token" {
  description = "Previder API Token"
  type        = string
}

variable "previder_config_url" {
  description = "Previder API Base URL (optional)"
  type        = string
  nullable    = true
}

variable "previder_config_customer" {
  description = "Previder API Customer ID (optional)"
  type        = string
  nullable    = true
}