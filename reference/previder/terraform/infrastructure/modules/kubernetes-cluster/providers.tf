terraform {
  required_providers {
    previder = {
      source  = "previder/previder"
      version = "~> 1.0"
    }
  }
}

provider "previder" {
  token = var.previder_config_token
  url = var.previder_config_url
  customer = var.previder_config_customer
}