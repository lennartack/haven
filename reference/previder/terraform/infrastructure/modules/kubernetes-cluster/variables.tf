variable "kubernetes_cluster_enabled" {
  description = "Enable Kubernetes Cluster"
  type        = bool
}

variable "kubernetes_cluster_name" {
  description = "Name of the cluster"
  type        = string
}

variable "kubernetes_cluster_cni" {
  description = "CNI to install"
  type        = string
}

variable "kubernetes_cluster_network" {
  description = "Network to deploy in (name or objectID)"
  type        = string
}

variable "kubernetes_cluster_vips" {
  description = "VIPS as comma seperated list"
  type        = list(string)
}

variable "kubernetes_cluster_endpoints" {
  description = "Endpoints as comma seperated list"
  type        = list(string)
}

variable "kubernetes_cluster_version" {
  description = "Version of the cluster (only when auto update is disabled)"
  type        = string
}

variable "kubernetes_cluster_auto_update" {
  description = "Automatically get the newest Kubernetes version"
  type        = bool
}

variable "kubernetes_cluster_auto_scale_enabled" {
  description = "Install an cluster autoscaler"
  type        = bool
}

variable "kubernetes_cluster_minimal_nodes" {
  description = "Minimal nodes"
  type        = number
}

variable "kubernetes_cluster_maximal_nodes" {
  description = "Maximal nodes"
  type        = number
}

variable "kubernetes_cluster_control_plane_cpu_cores" {
  description = "Number of cpu cores per node"
  type        = number
}

variable "kubernetes_cluster_control_plane_memory_gb" {
  description = "Number of memory GB per node"
  type        = number
}

variable "kubernetes_cluster_control_plane_storage_gb" {
  description = "Storage capacity per node (minimum 25)"
  type        = number
}

variable "kubernetes_cluster_node_cpu_cores" {
  description = "Number of cpu cores per node"
  type        = number
}

variable "kubernetes_cluster_node_memory_gb" {
  description = "Number of memory GB per node"
  type        = number
}

variable "kubernetes_cluster_node_storage_gb" {
  description = "Storage capacity per node (minimum 25)"
  type        = number
}

variable "kubernetes_cluster_compute_cluster" {
  description = "Compute cluster"
  type        = string
}

variable "kubernetes_cluster_high_available_control_plane" {
  description = "Install 1 or 3 control planes"
  type        = bool
}

variable "previder_config_token" {
  description = "Previder API Token"
  type        = string
}

variable "previder_config_url" {
  description = "Previder API Base URL (optional)"
  type        = string
  nullable =  true
}

variable "previder_config_customer" {
  description = "Previder API Customer ID (optional)"
  type        = string
  nullable =  true
}