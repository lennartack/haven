# Not implemented yet
variable "previder_config_token" {
  description = "Previder API Token"
  type        = string
}

variable "previder_config_url" {
  description = "Previder API Base URL (optional)"
  type        = string
  nullable =  true
}

variable "previder_config_customer" {
  description = "Previder API Customer ID (optional)"
  type        = string
  nullable =  true
}