# Reference Implementation: Haven on OpenStack

## Kops
Haven can easily be deployed on OpenStack using [Kops](https://kops.sigs.k8s.io/).

The [docker container](./kops) wraps everything together to easily work with a Haven cluster on OpenStack.

When ready ensure your new cluster is Haven Compliant by using the [Haven Compliancy Checker](../../haven/cli/README.md).

## Devstack
Development on this Reference Implementation can be assisted by deploying [Devstack](./devstack) resulting in a local OpenStack environment.

## License
Copyright © VNG Realisatie 2019-2023
Licensed under the EUPL
