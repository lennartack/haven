# Copyright © VNG Realisatie 2019-2023
# Licensed under the EUPL

#####################################################################################################################################

##### [ HAVEN REFERENCE IMPLEMENTATION: OPENSTACK ] #####
FROM        alpine

# Versions.
ARG         KOPS_VERSION=1.27.0
ARG         KUBERNETES_VERSION=1.27.4
ARG         HELM_VERSION=3.12.3
ARG         ANSIBLE_VERSION=8.3.0
ARG         PYTHON_OPENSTACKCLIENT_VERSION=6.2.0
ARG         PYTHON_NEUTRONCLIENT_VERSION=11.0.0
ARG         PYTHON_OCTAVIACLIENT_VERSION=3.4.0
ARG         PYTHON_SWIFTCLIENT_VERSION=4.3.0
ARG         METRICS_CHART_VERSION=6.4.5

# Exports used by provisioners.
ENV         KUBERNETES_VERSION=${KUBERNETES_VERSION}
ENV         METRICS_CHART_VERSION=${METRICS_CHART_VERSION}

# [ Base ]
RUN         mkdir -p /opt/haven
ENV         HOME=/opt/haven
WORKDIR     /opt/haven

RUN         apk add --no-cache bash bash-completion vim curl openssh-client build-base git python3 python3-dev py3-pip jq gcc openssl-dev libffi-dev musl-dev apache2-utils
RUN         pip3 install --upgrade --break-system-packages pip
RUN         mkdir -p /opt/haven/bin/helpers

# [ CLI ]
# Openstack.
RUN         pip3 install --break-system-packages python-openstackclient==${PYTHON_OPENSTACKCLIENT_VERSION} python-neutronclient==${PYTHON_NEUTRONCLIENT_VERSION} python-octaviaclient==${PYTHON_OCTAVIACLIENT_VERSION} python-swiftclient==${PYTHON_SWIFTCLIENT_VERSION}
# Kubernetes..
RUN         (curl -L -o /usr/local/bin/kubectl https://storage.googleapis.com/kubernetes-release/release/v${KUBERNETES_VERSION}/bin/linux/amd64/kubectl && chmod 755 /usr/local/bin/kubectl)
# Helm.
RUN         (curl -L https://get.helm.sh/helm-v${HELM_VERSION}-linux-amd64.tar.gz | tar -xzO linux-amd64/helm > /usr/local/bin/helm && chmod 755 /usr/local/bin/helm)
RUN         helm repo add stable https://charts.helm.sh/stable
RUN         helm repo add appscode https://charts.appscode.com/stable/
RUN         helm repo update

# [ Kops ]
RUN         (curl -L -o /usr/local/bin/kops https://github.com/kubernetes/kops/releases/download/v${KOPS_VERSION}/kops-linux-amd64 && chmod 755 /usr/local/bin/kops)
COPY        ./bin/ /opt/haven/bin/

# [ Bash ]
RUN         ln -s /opt/haven/bin/helpers/_bashrc /opt/haven/.bashrc
RUN         /usr/local/bin/kops completion bash >> /opt/haven/.bashrc
RUN         /usr/local/bin/kubectl completion bash >> /opt/haven/.bashrc
RUN         /usr/local/bin/helm completion bash >> /opt/haven/.bashrc
RUN         /usr/bin/openstack complete >> /opt/haven/.bashrc

# [ Secrets ]
RUN         pip3 install --break-system-packages ansible==${ANSIBLE_VERSION}

# [ Haven CLI ]
COPY        ./haven.zip /opt/haven/bin/haven.zip
RUN         unzip /opt/haven/bin/haven.zip

# [ Base ]
COPY        ./docker-entrypoint.sh /docker-entrypoint.sh
RUN         chmod 755 /docker-entrypoint.sh

ENTRYPOINT  ["/docker-entrypoint.sh"]
