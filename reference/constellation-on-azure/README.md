# Reference implementation: Constellation on Azure

This is a reference implementation that explains how to create a Haven-compliant Kubernetes cluster with Constellation in the Microsoft Azure Cloud.

## Constellation on Azure

Before you can start using the Constellation CLI, you should ensure that you fulfill the prerequisites.

## Prerequisites
- Your machine is running Linux or macOS
- [kubectl](https://kubernetes.io/docs/tasks/tools/) is installed
- [Helm](https://helm.sh/docs/intro/install/) is installed
- [Azure CLI](https://learn.microsoft.com/en-us/cli/azure/install-azure-cli) is installed, and you are logged in with the subscription you want to create the cluster in.

## How it works

Constellation can easily be deployed using the Constellation CLI. Instructions on how to [install the CLI](https://docs.edgeless.systems/constellation/getting-started/install) can be found in the online documentation.

After installing the CLI in your local shell environment we need two more steps to create your cluster.

1. Prepare the configuration file with the information about your Azure subscription such as the region and resource groups.
2. Run the provided deployment script that creates the cluster and configures it according to the Haven standard

### Prepare the configuration file

Follow the steps from the [online documentation](https://docs.edgeless.systems/constellation/getting-started/first-steps) to prepare your shell environment for your Azure subscription and fill in the Azure specific information into the configuration file [constellation-conf.yaml](./constellation-conf.yaml)

### Create the cluster

We've prepared a [deployment script](setup-constellation-with-deployments.sh) that creates a cluster based on the configuration file and prepares it to comply with the  Haven standard.
After you followed the steps from the documentation and filled out the missing values in the configuration file, you can run the deployment script in your terminal.
Note that the script assumes that the environment variables `RESOURCE_GROUP` and `LOCATION` are still set from the previous step (e.g. you are using the same terminal as before). In case they are not, redefine them first:

```bash
export RESOURCE_GROUP=constellation # enter name of new resource group for your cluster here
export LOCATION=westeurope # enter location of resources here
```

Execute the script as such:

```bash
chmod +x setup-constellation-with-deployments.sh
./setup-constellation-with-deployments.sh
```

The script will automatically set up a cluster that consists of three control plane nodes and three worker nodes. Constellation is initialized here in "Conformance mode" (`constellation init --conformance`). This applies a [few tweaks](https://github.com/edgelesssys/constellation/blob/1b64951488df9bfa439e5a3fd50b0b7e04e44f92/cli/internal/helm/loader.go#L63-L71) to [Cillium](https://cilium.io), the default CNI for Kubernetes to be CNCF Conformance compliant as Cillium tries to replace `kube-proxy` but by doing so, does not pass few tests currently by default without the change.

To reach Haven compliancy, the script will install the following additional applications in your cluster:

- ReadWriteMany storage using [Azure Files](https://azure.microsoft.com/en-us/products/storage/files/)
- [cert-manager](https://cert-manager.io) for automatic HTTPS provisioning
- [Prometheus](https://prometheus.io) for Kubernetes node metrics
- [Promtail](https://grafana.com/docs/loki/latest/clients/promtail/) & [Loki](https://grafana.com/oss/loki/) for in-cluster log aggregation
- [Grafana](https://grafana.com/grafana/) to monitor the node metrics and aggregated logs

Both Prometheus and Grafana are by default configured to create PersistentVolumes based on [Constellation's custom CSI driver](https://docs.edgeless.systems/constellation/architecture/encrypted-storage).
This CSI driver allows for the automatic creation of [Azure Managed Disks](https://azure.microsoft.com/en-us/pricing/details/managed-disks/) which are encrypted transparently on node level and can be used for ReadWriteOnce storage. 
By using this driver, Azure does not have access to the content or the encryption keys of the disk.

Note that using this specific selection of deployments is not required. 
You are free to pick other Haven compliant options to pass the deployment compliancy checks.

## Usage

After creating the cluster, point the `KUBECONFIG` variable to the created file:

```bash
export KUBECONFIG="$PWD/constellation-admin.conf"
```

Afterwards you can use `kubectl` to interact with your Kubernetes cluster.

### Deployments

#### Grafana

To access the deployment of Grafana, follow these steps:

1. Get your `admin` user password by running:

    ```bash
    kubectl get secret --namespace monitoring grafana -o jsonpath="{.data.admin-password}" | base64 --decode ; echo
    ```

2. Port-forward Grafana and make it accessible on [localhost:3000](http://localhost:3000)

    ```bash
    export POD_NAME=$(kubectl get pods --namespace monitoring -l "app.kubernetes.io/name=grafana,app.kubernetes.io/instance=grafana" -o jsonpath="{.items[0].metadata.name}")
    kubectl --namespace monitoring port-forward $POD_NAME 3000
    ```

3. Visit Grafana on [localhost:3000](http://localhost:3000) and login with the user name `admin` and the password from step 1.

You should see Loki and Prometheus already automatically being added to Grafana and can use Grafana to check on metrics received from Prometheus or logs collected by Loki & Promtail.


## Haven

### Running
Instructions on how to run the Haven Compliancy Checker can be found [here](https://haven.commonground.nl/techniek/compliancy-checker).


### Results
```
+----------------+---------------------------------------------------------------------------+--------+
|    CATEGORY    |                                   NAME                                    | PASSED |
+----------------+---------------------------------------------------------------------------+--------+
| Fundamental    | Self test: HCC version is latest stable or within 3 months upgrade window | YES    |
| Fundamental    | Self test: does HCC have cluster-admin                                    | YES    |
| Infrastructure | Multiple availability zones in use                                        | YES    |
| Infrastructure | Running at least 3 master nodes                                           | YES    |
| Infrastructure | Running at least 3 worker nodes                                           | YES    |
| Infrastructure | Nodes have SELinux, Grsecurity, AppArmor, LKRG or Talos enabled           | YES    |
| Infrastructure | Private networking topology                                               | YES    |
| Cluster        | Kubernetes version is latest stable or max 2 minor versions behind        | YES    |
| Cluster        | Role Based Access Control is enabled                                      | YES    |
| Cluster        | Basic auth is disabled                                                    | YES    |
| Cluster        | ReadWriteMany persistent volumes support                                  | YES    |
| Cluster        | LoadBalancer service type support                                         | YES    |
| External       | CNCF Kubernetes Conformance                                               | YES    |
| Deployment     | Automated HTTPS certificate provisioning                                  | YES    |
| Deployment     | Log aggregation is running                                                | YES    |
| Deployment     | Metrics-server is running                                                 | YES    |
+----------------+---------------------------------------------------------------------------+--------+

[I] Suggested checks results:

+----------+-----------------------------------+--------+
| CATEGORY |               NAME                | PASSED |
+----------+-----------------------------------+--------+
| External | CIS Kubernetes Security Benchmark | YES    |
+----------+-----------------------------------+--------+
```

## Cleanup
To terminate the cluster, first manually remove the `havendemoazurefiles` Azure Files storage in the Constellation resource group.

Then, simply run the following command locally:
```bash
constellation terminate
```
It will clean up the all other resources created as part of the `constellation create` command on Azure.
