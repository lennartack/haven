# Anthos Policy Controller

Sets up Anthos Policy Controller in the cluster (eg. managed Gatekeeper).

<!-- BEGIN TFDOC -->
## Variables

| name | description | type | required | default |
|---|---|:---: |:---:|:---:|
| cluster_id | Cluster ID | <code title="">string</code> | ✓ |  |
| cluster_name | Cluster name | <code title="">string</code> | ✓ |  |
| project_id | GCP project ID | <code title="">string</code> | ✓ |  |
| *acm_service_account* | Anthos Service Account | <code title="">string</code> |  | <code title=""></code> |
| *admin_email* | Deployment user email or service account email | <code title="">string</code> |  | <code title=""></code> |
| *enable_anthos_features* | Enable Anthos features (these are enabled once per project) | <code title="">bool</code> |  | <code title="">true</code> |
| *gkehub_service_account* | GKE Hub Service Account | <code title="">string</code> |  | <code title=""></code> |
| *install_cis_policies* | Install sample CIS Gatekeeper policies | <code title="">bool</code> |  | <code title="">true</code> |

## Outputs

| name | description | sensitive |
|---|---|:---:|
| installed | None |  |
<!-- END TFDOC -->
