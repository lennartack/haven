#   Copyright 2021 Google LLC
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

variable "project_id" {
  type        = string
  description = "GCP project ID"
}

variable "cluster_name" {
  type        = string
  description = "Cluster name"
}

variable "cluster_id" {
  type        = string
  description = "Cluster ID"
}

variable "install_cis_policies" {
  type        = bool
  description = "Install sample CIS Gatekeeper policies"
  default     = true
}

variable "enable_anthos_features" {
  type        = bool
  description = "Enable Anthos features (these are enabled once per project)"
  default     = true
}

variable "admin_email" {
  type        = string
  description = "Deployment user email or service account email"
  default     = ""
}

variable "acm_service_account" {
  type        = string
  description = "Anthos Service Account"
  default     = ""
}

variable "gkehub_service_account" {
  type        = string
  description = "GKE Hub Service Account"
  default     = ""
}
