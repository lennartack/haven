#   Copyright 2021 Google LLC
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

terraform {
  required_version = ">= 1.0.0"

  required_providers {
    google = {
      source  = "hashicorp/google"
      version = ">= 4.0.0"
    }
  }
}

locals {
  compliance_containers = [
    "docker.io/nginx/nginx:*",
    "docker.io/busybox/busybox:*",
    "docker.io/aquasec/kube-bench:*",
    "projects.registry.vmware.com/sonobuoy/sonobuoy:*",
    "k8s.gcr.io/conformance:*",
  ]
}

# Add Binary Authorization attestor note
resource "google_container_analysis_note" "attestor-note" {
  name    = format("%s-attestor-note", var.attestor_name)
  project = var.project_id

  attestation_authority {
    hint {
      human_readable_name = "Haven Sample Attestor"
    }
  }
}

# Add Binary Authorization attestotr
resource "google_binary_authorization_attestor" "attestor" {
  name    = format("%s-attestor", var.attestor_name)
  project = var.project_id

  attestation_authority_note {
    note_reference = google_container_analysis_note.attestor-note.name
  }
}

# Add Binary Authorization policy
resource "google_binary_authorization_policy" "binauthz-policy" {
  project = var.project_id

  dynamic "admission_whitelist_patterns" {
    for_each = var.compliance_testing_use ? concat(var.admission_allowlist, local.compliance_containers) : var.admission_allowlist
    content {
      name_pattern = admission_whitelist_patterns.value
    }
  }

  global_policy_evaluation_mode = "ENABLE"

  default_admission_rule {
    evaluation_mode         = "REQUIRE_ATTESTATION"
    enforcement_mode        = var.enforcement_mode
    require_attestations_by = [google_binary_authorization_attestor.attestor.name]
  }
}
