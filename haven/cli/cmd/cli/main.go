// Copyright © VNG Realisatie 2019-2024
// Licensed under EUPL v1.2

package main

import (
	_ "k8s.io/client-go/plugin/pkg/client/auth"
	"os"

	"gitlab.com/commonground/haven/haven/haven/cli/pkg/logging"
)

func main() {
	rootCmd := NewCmdHaven()

	if err := rootCmd.Execute(); err != nil {
		logging.Error("Running app: %s", err)
		os.Exit(1)
	}
}
