// Copyright © VNG Realisatie 2019-2024
// Licensed under EUPL v1.2

package main

import (
	"github.com/spf13/cobra"
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/compliancy"
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/logging"
	"os"
)

var (
	version = "undefined"
	logFile string
)

func NewCmdHaven() *cobra.Command {
	// rootCmd represents the base command when called without any subcommands
	rootCmd := &cobra.Command{
		Use:   "haven",
		Short: "A standard for platform-independent cloud hosting",
		Long:  "A standard for platform-independent cloud hosting",
	}

	rootCmd.PersistentFlags().StringVarP(&logFile, "log-file", "l", "", "Write logs to file (optional)")

	logging.LogFile = &logFile
	logging.Version = version
	compliancy.Version = version

	rootCmd.AddCommand(
		compliancy.NewCmdCompliancy(),
		compliancy.NewCmdVersion(),
	)

	rootCmd.SetHelpFunc(func(cmd *cobra.Command, args []string) {
		// Print the havenheader when a help command is run
		logging.WriteHavenHeader()
		rootCmd.SetHelpFunc(nil)
		if err := cmd.Help(); err != nil {
			logging.Fatal("Error returned from help command: '%s'", err.Error())
			os.Exit(1)
		}
	})

	return rootCmd
}
