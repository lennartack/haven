// Copyright © VNG Realisatie 2019-2024
// Licensed under EUPL v1.2

package compliancy

import (
	"context"
	"encoding/base64"
	"fmt"
	"k8s.io/api/storage/v1"
	"sort"
	"strconv"
	"strings"
	"time"

	"github.com/Masterminds/semver/v3"
	appsv1 "k8s.io/api/apps/v1"
	apiv1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	"gitlab.com/commonground/haven/haven/haven/cli/pkg/kubernetes"
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/logging"
)

func clusterVersion(ctx context.Context, config *Config) (Result, error) {
	lv, err := semver.NewVersion(config.KubeLatest)
	if err != nil {
		return ResultNo, fmt.Errorf("Kubernetes latest version formatting issue: %s, Error: %s", config.KubeLatest, err.Error())
	}

	maxThreeMinorBehind, _ := semver.NewConstraint(fmt.Sprintf(">= %d.%d", lv.Major(), lv.Minor()))

	sv := config.KubeServer

	*sv = sv.IncMinor().IncMinor().IncMinor()

	if maxThreeMinorBehind.Check(sv) {
		return ResultYes, nil
	}

	return ResultNo, nil
}

func clusterRBAC(ctx context.Context, config *Config) (Result, error) {
	groupList, err := config.Kube.Discovery().ServerGroups()
	if err != nil {
		return ResultNo, err
	}

	apiVersions := metav1.ExtractGroupVersions(groupList)

	sort.Strings(apiVersions)

	for _, v := range apiVersions {
		if "rbac.authorization.k8s.io/v1" == v {
			return ResultYes, nil
		}
	}

	return ResultNo, nil
}

func clusterBasicAuth(ctx context.Context, config *Config) (Result, error) {
	// Amazon EKS, Azure AKS and Google cloud GKE control plane does not appear in the nodes list and
	// are confirmed to have basic auth disabled.
	if config.HostPlatform == PlatformEKS {
		return ResultYes, nil
	}

	if config.HostPlatform == PlatformAKS {
		return ResultYes, nil
	}

	if config.HostPlatform == PlatformGKE {
		return ResultYes, nil
	}

	// Every distribution of Red Hat OpenShift is confirmed to have basic auth disabled.
	// This can be checked through a manual `oc debug node/$master_name`, there is no SSH access.
	if config.HostPlatform == PlatformOpenShift {
		return ResultYes, nil
	}

	logging.Info("Check basic auth: Using fallback method with pod(s) for 10-20 seconds.\n")

	out, err := kubernetes.ExecCmd(ctx, config.Kube, "ps uax | grep kube-apiserver",
		kubernetes.WithMaster(),
		kubernetes.WithNamespace(config.Namespace),
		kubernetes.WithTickInterval(config.GlobalInterval),
		kubernetes.WithShutdownTimeout(config.ShutdownTimeout))
	if err != nil {
		return ResultUnknown, err
	}

	if !strings.Contains(out, "--basic-auth-file") {
		return ResultYes, nil
	}

	return ResultNo, nil
}

func clusterVolumes(ctx context.Context, config *Config) (Result, error) {
	// ReadWriteMany as listed on https://kubernetes.io/docs/concepts/storage/storage-classes/
	// Possibly more at https://github.com/kubernetes-incubator/external-storage
	provisioners := [...]string{
		"ceph",
		"nfs",
		"manila",
		"efs",
		"azure-file",
		"glusterfs",
		"quobyte",
		"portworx-volume",
		"trident",
		"longhorn",
	}

	sc, err := config.Kube.StorageV1().StorageClasses().List(ctx, metav1.ListOptions{})
	if err != nil {
		return ResultNo, err
	}

	// If a known provisioner is found we're done quickly.
	for _, s := range sc.Items {
		for _, p := range provisioners {
			if strings.Contains(strings.ToUpper(s.Provisioner), strings.ToUpper(p)) {
				return ResultYes, nil
			}
		}
	}

	// Fallback: try to create a RWX volume with each storageclass.
	app := "hcc-test-" + strings.ToLower(base64.StdEncoding.EncodeToString([]byte(strconv.Itoa(time.Now().Nanosecond())))[:5])

	for _, s := range sc.Items {
		storageClassValid, err := fallBackStorageClass(ctx, app, s, config)
		if err != nil {
			logging.Error(err.Error())
		}
		if storageClassValid {
			return ResultYes, nil
		}
	}

	return ResultNo, nil
}

func fallBackStorageClass(ctx context.Context, app string, s v1.StorageClass, config *Config) (bool, error) {
	storageClassTestPassed := false

	logging.Info("Check ReadWriteMany: Using fallback method with \"%s\" storage class for 1-3 minutes.\n", s.Name)

	pvc := &apiv1.PersistentVolumeClaim{
		ObjectMeta: metav1.ObjectMeta{
			Name: app,
		},
		Spec: apiv1.PersistentVolumeClaimSpec{
			StorageClassName: &s.Name,
			AccessModes:      []apiv1.PersistentVolumeAccessMode{apiv1.ReadWriteMany},
			Resources: apiv1.VolumeResourceRequirements{
				Requests: apiv1.ResourceList{
					apiv1.ResourceStorage: resource.MustParse("1Gi"),
				},
			},
		},
	}

	if _, err := config.Kube.CoreV1().
		PersistentVolumeClaims(config.Namespace).
		Create(ctx, pvc, metav1.CreateOptions{}); err != nil {
		return storageClassTestPassed, err
	}

	defer func() {
		ctx, cancel := context.WithTimeout(context.Background(), config.ShutdownTimeout)
		defer cancel()

		if err := config.Kube.CoreV1().
			PersistentVolumeClaims(config.Namespace).
			Delete(ctx, app, metav1.DeleteOptions{}); err != nil {
			logging.Error("Delete PersistentVolumeClaim '%s': %s", app, err)
		}
	}()

	// We should expect a PVC to be created within 60 seconds.
	start := time.Now()
	ticker := time.NewTicker(config.GlobalInterval)
	defer ticker.Stop()

	for ts := range ticker.C {
		if ts.Sub(start).Seconds() >= 60 {
			return false, fmt.Errorf("FallBack method for RWX and SC %s: Time out expired PVC was NOT created", s.Name)
		}

		pvc, err := config.Kube.CoreV1().
			PersistentVolumeClaims(config.Namespace).
			Get(ctx, app, metav1.GetOptions{})
		if err != nil {
			return false, err
		}

		if "Bound" == pvc.Status.Phase {
			break
		}
	}

	replicas := int32(2)

	deploy := &appsv1.Deployment{
		ObjectMeta: metav1.ObjectMeta{
			Name: app,
		},
		Spec: appsv1.DeploymentSpec{
			Replicas: &replicas,
			Selector: &metav1.LabelSelector{
				MatchLabels: map[string]string{
					"app": app,
				},
			},
			Template: apiv1.PodTemplateSpec{
				ObjectMeta: metav1.ObjectMeta{
					Labels: map[string]string{
						"app": app,
					},
				},
				Spec: apiv1.PodSpec{
					Containers: []apiv1.Container{
						{
							Name:  "web",
							Image: "nginx:latest",
							VolumeMounts: []apiv1.VolumeMount{
								{
									Name:      "storage",
									MountPath: "/storage",
								},
							},
						},
					},
					Volumes: []apiv1.Volume{
						{
							Name: "storage",
							VolumeSource: apiv1.VolumeSource{
								PersistentVolumeClaim: &apiv1.PersistentVolumeClaimVolumeSource{
									ClaimName: app,
									ReadOnly:  false,
								},
							},
						},
					},
				},
			},
		},
	}

	_, err := config.Kube.AppsV1().
		Deployments(config.Namespace).
		Create(ctx, deploy, metav1.CreateOptions{})
	if err != nil {
		return false, err
	}

	defer func() {
		ctx, cancel := context.WithTimeout(context.Background(), config.ShutdownTimeout)
		defer cancel()

		if err := config.Kube.AppsV1().
			Deployments(config.Namespace).
			Delete(ctx, app, metav1.DeleteOptions{}); err != nil {
			logging.Error("Delete Deployment '%s': %s", app, err)
		}
	}()

	// We should expect 2 pods both running having the RWM PVC attached within 120 seconds.
	start = time.Now()
	ticker.Reset(5 * time.Second)
	for ts := range ticker.C {
		if ts.Sub(start).Seconds() >= 120 {
			return false, fmt.Errorf("FallBack method for RWX and SC %s: Time out expired deployment was not attached for class", s.Name)
		}

		label := fmt.Sprintf("app=%s", app)
		pods, err := config.Kube.CoreV1().
			Pods(config.Namespace).
			List(ctx, metav1.ListOptions{LabelSelector: label})
		if err != nil {
			return false, err
		}

		attached := 0
		for _, pod := range pods.Items {
			if pod.Status.Phase == "Running" {
				attached += 1

				if attached == 2 {
					return true, nil
				}
			}
		}
	}

	return false, nil
}
