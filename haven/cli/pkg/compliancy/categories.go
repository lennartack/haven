// Copyright © VNG Realisatie 2019-2024
// Licensed under EUPL v1.2

package compliancy

// Category a compliancy check is part of
type Category string
