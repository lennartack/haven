package compliancy

import (
	"context"
	"fmt"
	"github.com/Masterminds/semver/v3"
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/logging"
	"runtime"
)

func shaValidation(ctx context.Context, config *Config) (Result, error) {
	var currentVersion HavenRelease
	havenVersion, err := semver.NewVersion(Version)
	if err != nil {
		havenVersion, err = semver.NewVersion(config.HavenReleases.latest().Version)
		if err != nil {
			return ResultNo, fmt.Errorf("Could not determine the current running version so no hash can be comnpared: '%s'", err.Error())
		}
	}

	for _, release := range config.HavenReleases.Releases {
		if release.Version == havenVersion.Original() {
			currentVersion = release
			break
		}
	}

	remoteHash := getRemoteHash(currentVersion.Hashes)

	// remove when merging branch feature/v12
	logging.Info(fmt.Sprintf("found the following remote hash: %s in the releases.json", remoteHash))
	logging.Info(fmt.Sprintf("found current bin: %s comparing and sending result", config.CurrentBinHash))
	if remoteHash == config.CurrentBinHash {
		return ResultYes, nil
	}

	return ResultNo, nil
}

func getRemoteHash(h Hash) string {
	os := runtime.GOOS
	arch := runtime.GOARCH
	platform := fmt.Sprintf("%s/%s", os, arch)

	switch platform {
	case "darwin/arm64":
		return h.DarwinArm64
	case "linux/arm64":
		return h.LinuxArm64
	case "darwin/amd64":
		return h.DarwinAmd64
	case "linux/amd64":
		return h.LinuxAmd64
	case "windows/amd64":
		return h.WindowsAmd64
	default:
		return ""
	}
}
