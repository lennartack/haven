// Copyright © VNG Realisatie 2019-2024
// Licensed under EUPL v1.2

package compliancy

import (
	"context"
	"embed"
	"encoding/json"
	"fmt"
	"github.com/spf13/cobra"
	"io"
	"os"
	"os/signal"
	"strings"
	"sync/atomic"
	"time"

	"github.com/gookit/color"
	"sigs.k8s.io/yaml"

	"gitlab.com/commonground/haven/haven/haven/cli/pkg/kubernetes/v1alpha1"
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/logging"
)

var callingCommand = ""

func NewCmdCompliancy() *cobra.Command {
	var (
		rationaleFlag bool
		runCNCFChecks bool
		runCISChecks  bool
		runKESCChecks bool
		outputFormat  string
	)

	cmd := &cobra.Command{
		Use:   "check",
		Short: "Validates cluster for Haven Compliancy",
		Long:  "Runs a series of compliancy checks to make sure the cluster is Haven Compliant",
		Run: func(cmd *cobra.Command, args []string) {
			osArgs := os.Args

			logging.OutputFormat = &outputFormat
			logging.WriteHavenHeader()

			if outputFormat != "text" && outputFormat != "json" {
				logging.Fatal("Invalid output format: '%s'. Try 'text' or 'json'\n", outputFormat)

				return
			}

			callingCommand = strings.Join(osArgs, " ")
			if err := initChecker(); err != nil {
				fmt.Printf("Fatal error initializing Haven Compliancy Checker: %s.\n", err.Error())

				return
			}

			if err := initWhitelist(); err != nil {
				fmt.Printf("Fatal error initializing Haven Compliancy Checker: %s.\n", err.Error())

				return
			}

			if rationaleFlag {
				cmdRationale()

				return
			}

			cmdCheck(&runCNCFChecks, &runCISChecks, &runKESCChecks)

		},
	}
	cmd.PersistentFlags().BoolVarP(&rationaleFlag, "rationale", "", false, "[Compliancy] Displays the reasoning behind all Compliancy Checks (does not run the Checker).")
	cmd.PersistentFlags().BoolVarP(&runCNCFChecks, "cncf", "", true, "[Compliancy] Run external CNCF checks. Required, results in skipped test when set to false.")
	cmd.PersistentFlags().BoolVarP(&runCISChecks, "cis", "", false, "[Suggestion] Run external CIS checks by specifying log output file path. Optional.")
	cmd.PersistentFlags().BoolVarP(&runKESCChecks, "kubescape", "", false, "[Suggestion] Run external Kubescape checks by specifying log output file path. Optional.")
	cmd.PersistentFlags().StringVarP(&outputFormat, "output", "o", "text", "Specify output format (text, json)")

	return cmd
}

var (
	Version string

	checks    Checks
	output    CompliancyOutput
	rationale RationaleOutput
	whitelist Whitelist

	//go:embed static/checks.yaml
	embedFiles embed.FS
	//go:embed static/whitelist.yaml
	embedWhitelist embed.FS
	//go:embed static/kube-bench static/kubescape
	embedStatic embed.FS
)

// initChecker sources checks.yaml and preps output.
func initChecker() error {
	rationale.Version = fmt.Sprintf("Haven %s", Version)
	output.Version = fmt.Sprintf("Haven %s", Version)
	output.StartTS = time.Now()

	f, err := embedFiles.Open("static/checks.yaml")
	if err != nil {
		return err
	}

	defer f.Close()

	b, err := io.ReadAll(f)
	if err != nil {
		return err
	}

	err = yaml.Unmarshal(b, &checks)
	if err != nil {
		return err
	}

	return nil
}

// initWhitelist sources whitelist.yaml and preps output.
func initWhitelist() error {
	f, err := embedWhitelist.Open("static/whitelist.yaml")
	if err != nil {
		return err
	}

	defer f.Close()

	b, err := io.ReadAll(f)
	if err != nil {
		return err
	}

	err = yaml.Unmarshal(b, &whitelist)
	if err != nil {
		return err
	}

	return nil
}

// cmdCheck validates requirements, runs the in scope checks and outputs
// results.
func cmdCheck(runCNCFChecks, runCISChecks, runKESCChecks *bool) {
	logging.Debug("HAVEN COMPLIANCY CHECKER - STARTED\n")
	defer logging.Debug("HAVEN COMPLIANCY CHECKER - FINISHED\n\n\n")

	logging.Info("Initializing at %s UTC", time.Now().UTC().Format(time.RFC3339))

	output.Config.CNCF = *runCNCFChecks

	if *runCNCFChecks {
		logging.Info("Preflight: CNCF: Opted in. Enabled external CNCF compliancy check.\n")
	} else {
		logging.WarningYellow("Preflight: CNCF: Not opted in. Skipping external CNCF compliancy check.")
	}

	if *runCISChecks {
		logging.Info("Preflight: CIS: Opted in. Enabled external CIS suggested check.\n")
	} else {
		logging.Info("Preflight: CIS: Not opted in. Skipping external CIS suggested check.\n")
	}

	output.Config.CIS = *runCISChecks

	if *runKESCChecks {
		logging.Info("Preflight: Kubescape: Opted in. Enabled external Kubescape suggested check.\n")
	} else {
		logging.Info("Preflight: Kubescape: Not opted in. Skipping external Kubescape suggested check.\n")
	}

	output.Config.KESC = *runKESCChecks

	checker, err := NewChecker(*runCNCFChecks, *runCISChecks, *runKESCChecks)
	if err != nil {
		logging.Fatal("Could not initialize checker: %s\n", err.Error())
	}

	logging.Info("Cluster ID: %s", checker.config.ClusterID)
	logging.Info("Kubernetes host platform: %s", checker.config.HostPlatform)
	logging.Info("Kubernetes server version: v%s", checker.config.KubeServer.String())
	logging.Info("Latest Haven release: %s", checker.config.HavenReleases.latest().Version)
	logging.Info("Latest Kubernetes release: %s", checker.config.KubeLatest)

	logging.Info("Detecting Haven CRD and creating if necessary")
	created, err := checker.config.CrdClient.CreateHavenDefinition()
	if err != nil {
		logging.Fatal("Error occured while detecting or deploying Haven CRD: %s", err.Error())
	}
	if created {
		logging.Info("Haven CRD was not installed yet and has now been deployed")
	}

	var clean atomic.Bool

	ctx, stop := signal.NotifyContext(context.Background(), os.Interrupt, os.Kill)
	defer func() {
		clean.Store(true)
		stop()
	}()

	go func() {
		<-ctx.Done()
		if !clean.Load() {
			logging.Info("Received shutdown signal. Exiting Haven Compliancy Checker gracefully...")
		}
	}()

	logging.Info(color.Bold.Sprintf("Running checks in parallel..."))

	if err := checker.Run(ctx); err != nil {
		logging.Error("Checker encountered an error: %s\n", err.Error())
		return
	}

	output.CompliancyChecks.Results = checks.CompliancyChecks
	output.SuggestedChecks.Results = checks.SuggestedChecks
	output.StopTS = time.Now()

	checker.PrintResults(checks.CompliancyChecks, true)
	checker.PrintResults(checks.SuggestedChecks, false)

	oj, err := outputJson(output)
	if err != nil {
		if *logging.OutputFormat == "json" {
			r, _ := json.Marshal(struct {
				Error string `json:",omitempty"`
			}{Error: err.Error()})
			oj = fmt.Sprint(string(r))

			fmt.Println(oj)
			os.Exit(1)
		}

		logging.Fatal("Could not build JSON output: %s", err)
	} else {
		if *logging.OutputFormat == "json" {
			fmt.Println(oj)
		}
	}

	compliant := output.CompliancyChecks.Summary.Total == output.CompliancyChecks.Summary.Passed

	input := v1alpha1.CompliancyInput{
		Commandline: callingCommand,
		KubeHost:    checker.config.Kube.Host(),
		Platform:    *checker.platform,
	}

	logging.Info("Persisting results to cluster through the CRD")
	if err := persistInCluster(checker.config.CrdClient, compliant, oj, input); err != nil {
		logging.Fatal("Could not persist output in cluster: %s", err)
	}
}

// cmdRationale outputs a table based on checks.yaml with all Compliancy and Suggested checks
// with the reasoning behind these checks.
func cmdRationale() {
	logging.Info("The Haven Compliancy Checker contains %d required Compliancy Checks to validate a cluster.", len(checks.CompliancyChecks))
	logging.Info("Compliancy Checks are restricted to essentials allowing for maximum flexibility while keeping the Haven promise of interoperability.")
	logging.Info("Besides the required checks there are %d Suggested Checks which can assist further enhancement of Haven environments.\n\n\n", len(checks.SuggestedChecks))

	logging.Info("** Compliancy Checks **\n\n")
	for _, c := range checks.CompliancyChecks {
		logging.Info(fmt.Sprintf("%s\n%s\n\n", c.Label, c.Rationale))
	}

	logging.Info("** Suggested Checks **\n\n")
	for _, s := range checks.SuggestedChecks {
		logging.Info(fmt.Sprintf("%s\n%s\n\n", s.Label, s.Rationale))
	}

	if *logging.OutputFormat == "json" {
		rationale.CompliancyChecks = checks.CompliancyChecks
		rationale.SuggestedChecks = checks.SuggestedChecks

		oj, err := outputJson(rationale)
		if err != nil {
			logging.Error("Could not print JSON output: %s", err)
		} else {
			fmt.Println(oj)
		}
	}
}
