package compliancy

import (
	"context"
	"encoding/base64"
	"fmt"
	"strconv"
	"strings"
	"testing"
	"time"

	"github.com/Masterminds/semver/v3"
	"github.com/stretchr/testify/assert"
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/kubernetes"
)

func TestSuggestionExternalCIS(t *testing.T) {
	version := "v1.21.5"
	namespace := "test"
	nameSuffix := strings.ToLower(base64.StdEncoding.EncodeToString([]byte(strconv.Itoa(time.Now().Nanosecond())))[:5])

	generateName = func(prefix string) string {
		return fmt.Sprintf("%s-%s", prefix, nameSuffix)
	}

	globalInterval := 1 * time.Millisecond
	testClient := kubernetes.NewFakeClientset()

	kubeServer, err := semver.NewVersion(version)
	assert.Nil(t, err)

	err = kubernetes.CreatePodForTest(fmt.Sprintf("kube-bench-%s", nameSuffix), namespace, testClient)
	assert.Nil(t, err)

	t.Run("PlatformUnknown", func(t *testing.T) {
		config := Config{
			Kube:           testClient,
			HostPlatform:   PlatformUnknown,
			KubeServer:     kubeServer,
			GlobalInterval: globalInterval,
			Namespace:      namespace,
		}

		result, err := suggestionExternalCIS(context.Background(), &config)
		assert.Nil(t, err)
		// since the logs are always "fake LOGS" the result will always be NO
		assert.Equal(t, result, ResultNo)
	})

	t.Run("PlatformGKE", func(t *testing.T) {
		config := Config{
			Kube:           testClient,
			HostPlatform:   PlatformGKE,
			KubeServer:     kubeServer,
			GlobalInterval: globalInterval,
			Namespace:      namespace,
		}

		result, err := suggestionExternalCIS(context.Background(), &config)
		assert.Nil(t, err)
		// since the logs are always "fake LOGS" the result will always be NO
		assert.Equal(t, result, ResultNo)
	})

	t.Run("PlatformEKS", func(t *testing.T) {
		config := Config{
			Kube:           testClient,
			HostPlatform:   PlatformEKS,
			KubeServer:     kubeServer,
			GlobalInterval: globalInterval,
			Namespace:      namespace,
		}

		result, err := suggestionExternalCIS(context.Background(), &config)
		assert.Nil(t, err)
		// since the logs are always "fake LOGS" the result will always be NO
		assert.Equal(t, result, ResultNo)
	})

	t.Run("PlatformAKS", func(t *testing.T) {
		config := Config{
			Kube:           testClient,
			HostPlatform:   PlatformAKS,
			KubeServer:     kubeServer,
			GlobalInterval: globalInterval,
			Namespace:      namespace,
		}

		result, err := suggestionExternalCIS(context.Background(), &config)
		assert.Nil(t, err)
		// since the logs are always "fake LOGS" the result will always be NO
		assert.Equal(t, result, ResultNo)
	})

}
